namespace Logic.CoreDomain.Component
{
    public static class TransformFactory
    {
        public static ITransform Create(
            Common.DataValues.Common.Vector3 position,
            Common.DataValues.Common.Quaternion rotation,
            Common.DataValues.Common.Vector3 scale)
            =>
            new Transform(position, rotation, scale);

        internal static Transform Clone(ITransform transform)
            =>
            new Transform(transform.Position, transform.Rotation, transform.Scale);
    }

}
