
namespace Logic.CoreDomain.Object3d
{
    public interface IObject3dRepository : Common.BaseDomain.Repository.IRepository<IObject3dEntity>
    {
    }
}
