using Logic.Common.BaseDomain.Data;
using Logic.Common.DataValues.Common;
using Logic.Common.DataValues.Gradient;
using Logic.CoreDomain.Component;

namespace Logic.CoreDomain.Object3d
{
    public static class Object3dFactory
    {
        public static IObject3dEntity Create(
            ID<IObject3dEntity> id,
            ITransform transform,
            Url url,
            Logic.Common.DataValues.Gradient.Color color)
        {
            return new Object3dEntity(id, transform, url, color);
        }
    }
}
