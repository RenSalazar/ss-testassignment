using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Component;

namespace Logic.CoreDomain.Object3d.Event
{
    public sealed class TransformSet : Shared.Event.BaseWithTransform<IObject3dEntity, IObject3dEntity>
    {
        public TransformSet(
            ID<IObject3dEntity> entityChanged,
            ID<IObject3dEntity> entityInitiatorId,
            ITransform entityChangeInitiatorTransform)
            : base(entityChanged, entityInitiatorId, entityChangeInitiatorTransform)
        {
        }
    }
}
