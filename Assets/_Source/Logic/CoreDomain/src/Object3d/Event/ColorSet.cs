﻿using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Component;
using Logic.Common.DataValues.Gradient;

namespace Logic.CoreDomain.Object3d.Event
{
    public sealed class ColorSet : Shared.Event.BaseWithColor<IObject3dEntity, IObject3dEntity>
    {
        public ColorSet(
            ID<IObject3dEntity> entityChanged,
            ID<IObject3dEntity> entityInitiatorId,
            Color color)
            : base(entityChanged, entityInitiatorId, color)
        {
        }
    }
}
