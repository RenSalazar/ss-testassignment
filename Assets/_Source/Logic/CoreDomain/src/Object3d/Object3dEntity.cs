using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Event;
using Logic.Common.DataValues.Common;
using Logic.Common.DataValues.Gradient;
using Logic.CoreDomain.Component;

namespace Logic.CoreDomain.Object3d
{
    public interface IObject3dEntity : Common.BaseDomain.Entity.IBaseEntity<IObject3dEntity>
    {
        ITransform Transform { get; }
        Url Url { get; }
        Color Color { get; }

        void SetTransform(ITransform transform);
        void SetColor(Color color);
    }

    internal sealed class Object3dEntity : Common.BaseDomain.Entity.Entity<IObject3dEntity>, IObject3dEntity
    {
        public ITransform Transform { get; private set; }
        public Url Url { get; }
        public Color Color { get; private set;  }

        public Object3dEntity(
            ID<IObject3dEntity> id,
            ITransform transform,
            Url url,
            Color color)
            : base(id)
        {
            Transform = transform;
            Url = url;
            Color = color;
        }

        public void SetTransform(ITransform transform)
        {
            Transform = transform;
            DomainEvents.Add(new Event.TransformSet(Id, Id, transform));
        }

        public void SetColor(Color color)
        {
            Color = color;
            DomainEvents.Add(new Event.ColorSet(Id, Id, color));
        }
    }
}
