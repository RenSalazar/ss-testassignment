using System.Threading;
using Logic.Common.BaseDomain.Data;
using Logic.Common.DataValues.Common;
using Logic.Common.DataValues.Gradient;

namespace Logic.CoreDomain.Shared.Event
{
    public abstract class Base<TEntityChanged> : Common.BaseDomain.Event.IDomainEvent
    {
        public CancellationToken CancellationToken { get; }
        public ID<TEntityChanged> EntityChanged { get; }

        protected Base(ID<TEntityChanged> entityChanged)
        {
            EntityChanged = entityChanged;
        }
        protected Base(ID<TEntityChanged> entityChanged, CancellationToken cancellationToken)
            : this(entityChanged)
        {
            CancellationToken = cancellationToken;
        }
    }

    public abstract class Base<TEntityChanged, TEntityChangeInitiator> : Base<TEntityChanged>
    {
        public ID<TEntityChangeInitiator> EntityChangeInitiatorId { get; }

        protected Base(ID<TEntityChanged> entityChanged, ID<TEntityChangeInitiator> entityChangeInitiatorId) :
            base(entityChanged)
        {
            EntityChangeInitiatorId = entityChangeInitiatorId;
        }
        protected Base(ID<TEntityChanged> entityChanged, ID<TEntityChangeInitiator> entityChangeInitiatorId, CancellationToken cancellationToken) :
            base(entityChanged, cancellationToken)
        {
            EntityChangeInitiatorId = entityChangeInitiatorId;
        }
    }

    public abstract class BaseWithTransform<TEntityChanged, TEntityChangeInitiator> : Base<TEntityChanged>
    {
        public ID<TEntityChangeInitiator> EntityChangeInitiatorId { get; }
        public Component.ITransform EntityChangeInitiatorTransform { get; }

        protected BaseWithTransform(ID<TEntityChanged> entityChanged, ID<TEntityChangeInitiator> entityInitiatorId, Component.ITransform entityChangeInitiatorTransform) :
            base(entityChanged)
        {
            EntityChangeInitiatorId = entityInitiatorId;
            EntityChangeInitiatorTransform = entityChangeInitiatorTransform;
        }
    }

    public abstract class BaseWithUrl<TEntityChanged> : Base<TEntityChanged>
    {
        public Url Url { get; }

        protected BaseWithUrl(ID<TEntityChanged> entityChanged, Url url) :
            base(entityChanged)
        {
            Url = url;
        }
    }

    public abstract class BaseWithColor<TEntityChanged, TEntityChangeInitiator> : Base<TEntityChanged>
    {
        public ID<TEntityChangeInitiator> EntityChangeInitiatorId { get; }
        public Color Color { get; }

        protected BaseWithColor(ID<TEntityChanged> entityChanged, ID<TEntityChangeInitiator> entityInitiatorId, Color color) :
            base(entityChanged)
        {
            EntityChangeInitiatorId = entityInitiatorId;
            Color = color;
        }
    }
}
