using System.Threading;
using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Object3d;

namespace Logic.CoreDomain.Beat.Event
{
    public sealed class Object3dAdded : Shared.Event.Base<IBeatEntity, IObject3dEntity>
    {
        public Object3dAdded(ID<IBeatEntity> entityChanged, ID<IObject3dEntity> id, CancellationToken cancellationToken) :
            base(entityChanged, id, cancellationToken)
        {
        }
    }
}
