using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Object3d;

namespace Logic.CoreDomain.Beat.Event
{
    public sealed class Object3dRemoved : Shared.Event.Base<IBeatEntity, IObject3dEntity>
    {
        public Object3dRemoved(ID<IBeatEntity> entityChanged, ID<IObject3dEntity> object3dId) :
            base(entityChanged, object3dId)
        {
        }
    }
}
