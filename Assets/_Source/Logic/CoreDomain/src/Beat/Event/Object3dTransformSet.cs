using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Component;
using Logic.CoreDomain.Object3d;

namespace Logic.CoreDomain.Beat.Event
{
    public sealed class Object3dTransformSet : Shared.Event.BaseWithTransform<IBeatEntity, IObject3dEntity>
    {
        public int Index { get; }

        public Object3dTransformSet(
            ID<IBeatEntity> entityChanged,
            ID<IObject3dEntity> entityInitiatorId,
            ITransform entityChangeInitiatorTransform,
            int index) : base(entityChanged, entityInitiatorId, entityChangeInitiatorTransform)
        {
            Index = index;
        }
    }
}
