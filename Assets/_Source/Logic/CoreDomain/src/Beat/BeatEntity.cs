using System.Collections.Generic;
using System.Threading;
using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Event;
using Logic.CoreDomain.Object3d;

namespace Logic.CoreDomain.Beat
{
    public interface IBeatEntity : Common.BaseDomain.Entity.IBaseEntity<IBeatEntity>
    {
        System.DateTime CreationDate { get; }

        IReadOnlyList<ID<IObject3dEntity>> Objects3d { get; }
        void AddObject3d(ID<IObject3dEntity> id, CancellationToken cancellationToken);
        void RemoveObject3d(ID<IObject3dEntity> id);
    }

    internal sealed class BeatEntity : Common.BaseDomain.Entity.Entity<IBeatEntity>, IBeatEntity
    {
        public System.DateTime CreationDate { get; private set; } = System.DateTime.UtcNow;
        public IReadOnlyList<ID<IObject3dEntity>> Objects3d => mObjects3d;

        private readonly List<ID<IObject3dEntity>> mObjects3d = new List<ID<IObject3dEntity>>();

        internal BeatEntity(ID<IBeatEntity> id) :
            base(id)
        {
        }

        internal BeatEntity(
            ID<IBeatEntity> id,
            List<ID<IObject3dEntity>> objects3d,
            System.DateTime creationDate) :
            this(id)
        {
            mObjects3d = objects3d;
            CreationDate = creationDate;
        }

        public void AddObject3d(ID<IObject3dEntity> id, CancellationToken cancellationToken)
        {
            mObjects3d.Add(id);
            DomainEvents.Add(new Event.Object3dAdded(Id, id, cancellationToken));
        }
        public void RemoveObject3d(ID<IObject3dEntity> id)
        {
            mObjects3d.Remove(id);
            DomainEvents.Add(new Event.Object3dRemoved(Id, id));
        }
    }
}
