using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Logic.Common.BaseDomain.Data;

namespace Logic.CoreDomain.Beat
{
    public interface IBeatRepository : Common.BaseDomain.Repository.IRepository<IBeatEntity>
    {
        Task<IReadOnlyList<IBeatEntity>> GetLastCreatedItemsAsync(int firstItemsCount, CancellationToken cancellationToken);
        Task<IReadOnlyList<IBeatEntity>> GetNextLastCreatedItemsAsync(ID<IBeatEntity> startAfterId, int firstItemsCount, CancellationToken cancellationToken);
    }
}
