using System;
using System.Collections.Generic;
using Logic.Common.BaseDomain.Data;
using Logic.Common.DataValues.Beat;
using Logic.CoreDomain.Object3d;

namespace Logic.CoreDomain.Beat
{
    public static class BeatFactory
    {
        public static IBeatEntity Create(ID<IBeatEntity> id)
        {
            BeatEntity beat = new BeatEntity(id);
            return beat;
        }

        public static IBeatEntity OnlyInConverterAppServiceCreateAndHere(
            ID<IBeatEntity> id,
            List<ID<IObject3dEntity>> objects3d,
            DateTime creationDate)
        {
            return new BeatEntity(
                id,
                objects3d,
                creationDate);
        }
    }
}
