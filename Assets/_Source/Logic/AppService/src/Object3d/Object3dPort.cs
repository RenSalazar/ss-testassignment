using Logic.AppService.Common.DTO;
using Logic.AppService.Context;
using Logic.Common.BaseDomain.Event;
using Logic.CoreDomain.Object3d;
using Logic.CoreDomain.Object3d.Event;

namespace Logic.AppService.Object3d
{
    public sealed class Object3dPort : System.IDisposable
    {
        public string Id { get { return mEntity.Id.Serialize(); } }

        private readonly IContext mContext;
        private readonly IObject3dEntity mEntity;
        private IObject3dPortListener _listener;
        private Common.EventHandler.CallbackFor<TransformSet> _onTransformSet;
        private Common.EventHandler.CallbackFor<ColorSet> _onColorSet;

        internal Object3dPort(IContext context, IObject3dEntity object3DEntity)
        {
            mContext = context;
            mEntity = object3DEntity;

            DomainEvents.Subscribe(_onTransformSet = new Common.EventHandler.CallbackFor<TransformSet>(OnTransformSet));
            DomainEvents.Subscribe(_onColorSet = new Common.EventHandler.CallbackFor<ColorSet>(OnColorSet));
        }
        public void Dispose()
        {
            DomainEvents.UnSubscribe(_onTransformSet);
            DomainEvents.UnSubscribe(_onColorSet);
        }
        public void Subscribe(IObject3dPortListener listener)
        {
            if (_listener != null)
            {
                throw new System.InvalidOperationException("Current listener is not null");
            }
            if (_listener == listener)
            {
                throw new System.InvalidOperationException("Current listener was allready subscribed");
            }
            _listener = listener;
        }
        public void Unsubscribe(IObject3dPortListener listener)
        {
            if (_listener == null)
            {
                throw new System.InvalidOperationException("Current listener is null");
            }
            if (_listener != listener)
            {
                throw new System.InvalidOperationException("Trying to unsubscribe for different listener");
            }
            _listener = null;
        }

        public DTO.Object3d GetData()
        {
            return DTO.Converter.ToDataTransferObject(mEntity);
        }
        public string GetUrl()
        {
            return mEntity.Url.Serialize();
        }
        public void SetTransform(Transform transform)
        {
            mEntity.SetTransform(Converter.ToTransformEntityComponent(transform));
            DomainEvents.Fire();
        }
        public void SetColor(Logic.Common.DataValues.Gradient.Color color)
        {
            mEntity.SetColor(color);
            DomainEvents.Fire();
        }

        private async void OnTransformSet(TransformSet domainEvent)
        {
            if (domainEvent.EntityChanged != mEntity.Id)
            {
                return;
            }
            await mContext.Object3dRepository.CommitAsync(mEntity);
            _listener.OnTransformSet(Common.DTO.Converter.ToTransformDTO(domainEvent.EntityChangeInitiatorTransform));
        }

        private async void OnColorSet(ColorSet domainEvent)
        {
            if (domainEvent.EntityChanged != mEntity.Id)
            {
                return;
            }
            await mContext.Object3dRepository.CommitAsync(mEntity);
            _listener.OnColorSet(Converter.ToDTOColor(domainEvent.Color));
        }
    }
}
