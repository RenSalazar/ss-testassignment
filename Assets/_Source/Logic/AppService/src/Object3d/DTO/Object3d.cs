namespace Logic.AppService.Object3d.DTO
{
    [System.Serializable]
    public struct Object3d
    {
        public string Id;
        public Common.DTO.Transform Transform;
        public string Url;
        public Common.DTO.Gradient.Color Color;

        public Object3d(
            string id,
            Common.DTO.Transform transform,
            string url,
            Common.DTO.Gradient.Color color)
        {
            Id = id;
            Transform = transform;
            Url = url;
            Color = color;
        }
    }
}
