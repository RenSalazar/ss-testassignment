using Logic.Common.BaseDomain.Data;
using Logic.Common.DataValues.Common;
using Logic.CoreDomain.Object3d;

namespace Logic.AppService.Object3d.DTO
{
    public static class Converter
    {
        public static IObject3dEntity ToEntity(Object3d dto)
        {
            return Object3dFactory.Create(
                new ID<IObject3dEntity>(dto.Id),
                Common.DTO.Converter.ToTransformEntityComponent(dto.Transform),
                new Url(dto.Url),
                Common.DTO.Converter.ToDomainColor(dto.Color));
        }

        public static Object3d ToDataTransferObject(IObject3dEntity entity)
        {
            var transform = entity.Transform;
            return new Object3d(
                entity.Id.Serialize(),
                new Common.DTO.Transform(
                    new Common.DTO.Vector3(transform.Position.X, transform.Position.Y, transform.Position.Z),
                    new Common.DTO.Quaternion(transform.Rotation.X, transform.Rotation.Y, transform.Rotation.Z, transform.Rotation.W),
                    new Common.DTO.Vector3(transform.Scale.X, transform.Scale.Y, transform.Scale.Z)),
                entity.Url.Serialize(),
                new Common.DTO.Gradient.Color(entity.Color.Red, entity.Color.Green, entity.Color.Blue, entity.Color.Alpha));
        }
    }
}
