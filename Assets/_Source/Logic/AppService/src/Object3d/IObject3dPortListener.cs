using System.Threading;

namespace Logic.AppService.Object3d
{
    public interface IObject3dPortListener
    {
        void OnTransformSet(Common.DTO.Transform transform);
        void OnColorSet(Common.DTO.Gradient.Color color);
    }
}
