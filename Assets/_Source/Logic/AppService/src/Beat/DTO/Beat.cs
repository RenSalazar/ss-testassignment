
namespace Logic.AppService.Beat.DTO
{
    [System.Serializable]
    public struct Beat
    {
        public string Id;
        public string[] Object3dIds;
        public long CreationDate;

        public Beat(
            string id,
            string[] object3dIds,
            long creationDate)
        {
            Id = id;
            Object3dIds = object3dIds;
            CreationDate = creationDate;
        }
    }
}
