using Logic.Common.BaseDomain.Data;
using Logic.CoreDomain.Beat;
using Logic.CoreDomain.Object3d;

namespace Logic.AppService.Beat.DTO
{
    public static class Converter
    {
        public static Beat ToDataTransferObject(IBeatEntity beat)
        {
            return new Beat(
                beat.Id.Serialize(),
                Common.DTO.Converter.ToStringIdArray(beat.Objects3d),
                Common.DTO.Converter.DateTimeToTimeStamp(beat.CreationDate)
            );
        }

        internal static Weather ToDataTransferObject(Logic.Common.DataValues.Beat.Weather weather)
        {
            return new Weather(weather.SerializeUrl(), weather.SerializeIntensity());
        }

        public static IBeatEntity ToEntity(Beat beatDTO)
        {
            var beatId = new ID<IBeatEntity>(beatDTO.Id);
            var result = CoreDomain.Beat.BeatFactory.OnlyInConverterAppServiceCreateAndHere(
                beatId,
                Common.DTO.Converter.ToEntityIdList<IObject3dEntity>(beatDTO.Object3dIds),
                Common.DTO.Converter.TimeStampToDateTime(beatDTO.CreationDate)
            );
            return result;
        }

        private static Logic.Common.DataValues.Beat.Weather ToDomainDataValue(Weather weather)
        {
            return new Logic.Common.DataValues.Beat.Weather(weather.Url, weather.Intensity);
        }
    }
}
