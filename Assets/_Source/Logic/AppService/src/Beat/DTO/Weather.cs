namespace Logic.AppService.Beat.DTO
{
    [System.Serializable]
    public struct Weather
    {
        public string Url;
        public float Intensity;

        public Weather(string url, float intensity)
        {
            Url = url;
            Intensity = intensity;
        }
    }
}
