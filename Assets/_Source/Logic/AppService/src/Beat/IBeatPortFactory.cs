using System.Threading;
using System.Threading.Tasks;

namespace Logic.AppService.Beat
{
    public interface IBeatPortFactory
    {
        Task<BeatPort> TryToCreateNextBeatPort(CancellationToken cancellationToken);
    }
}
