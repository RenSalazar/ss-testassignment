using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Logic.AppService.Object3d;
using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Event;
using Logic.Common.DataValues.Beat;
using Logic.CoreDomain.Beat;
using Logic.CoreDomain.Beat.Event;

namespace Logic.AppService.Beat
{
    public interface IBeatEntityProvider
    {
        IBeatEntity BeatEntity { get; }
    }

    public sealed class BeatPort : IBeatEntityProvider, System.IDisposable
    {
        IBeatEntity IBeatEntityProvider.BeatEntity => mBeatEntity;

        public string Id { get { return mBeatEntity.Id.Serialize(); } }

        private readonly IBeatEntity mBeatEntity;

        private Common.EventHandler.CallbackFor<Object3dAdded> _onObject3dAdded;
        private Common.EventHandler.CallbackFor<Object3dRemoved> _onObject3dRemoved;

        private readonly Context.IContext mContext;
        private IBeatPortListener _listener;

        internal BeatPort(Context.IContext context, IBeatEntity beat)
        {
            mContext = context;
            mBeatEntity = beat;
        }
        public void SetListener(IBeatPortListener listener)
        {
            _listener = listener;
            DomainEvents.Subscribe(_onObject3dAdded = new Common.EventHandler.CallbackFor<Object3dAdded>(OnObject3dAddedAsync));
            DomainEvents.Subscribe(_onObject3dRemoved = new Common.EventHandler.CallbackFor<Object3dRemoved>(OnObject3dRemoved));
        }
        public void Dispose()
        {
            DomainEvents.UnSubscribe(_onObject3dAdded);
            DomainEvents.UnSubscribe(_onObject3dRemoved);
        }

        public async void Initialize3dObjectsAsync(CancellationToken cancellationToken)
        {
            var object3dPorts = new Dictionary<string, Object3dPort>(mBeatEntity.Objects3d.Count);
            try
            {
                for (int i = 0; i < mBeatEntity.Objects3d.Count; i++)
                {
                    var object3dId = mBeatEntity.Objects3d[i];
                    var entity = await mContext.Object3dRepository.FindAsync(object3dId, cancellationToken);
                    var object3dPort = new Object3dPort(mContext, entity);
                    object3dPorts.Add(object3dId.Serialize(), object3dPort);
                }
                _listener.OnObjects3dInitialized(object3dPorts, cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                //TODO: add log message
            }
        }
        public string GetCreatedTimeAgoString()
        {
            return Logic.Common.Support.TimeAgo.Calculate(mBeatEntity.CreationDate);
        }
        private async void OnObject3dAddedAsync(Object3dAdded domainEvent)
        {
            if (domainEvent.EntityChanged != mBeatEntity.Id)
            {
                return;
            }
            await mContext.BeatRepository.CommitAsync(mBeatEntity);
            var entity = await mContext.Object3dRepository.FindAsync(domainEvent.EntityChangeInitiatorId, domainEvent.CancellationToken);
            Object3dPort object3dPort = new Object3dPort(mContext, entity);
            _listener.OnObject3dAdded(object3dPort, domainEvent.CancellationToken);
        }
        private async void OnObject3dRemoved(Object3dRemoved domainEvent)
        {
            if (domainEvent.EntityChanged != mBeatEntity.Id)
            {
                return;
            }
            string entityRemovedId = domainEvent.EntityChangeInitiatorId.Serialize();
            await mContext.BeatRepository.CommitAsync(mBeatEntity);
            _listener.OnObject3dRemoved(entityRemovedId);
        }
    }
}
