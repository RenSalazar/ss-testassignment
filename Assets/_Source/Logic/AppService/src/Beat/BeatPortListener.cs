using System.Collections.Generic;
using System.Threading;
using Logic.AppService.Object3d;

namespace Logic.AppService.Beat
{
    public interface IBeatPortListener
    {
        void OnObjects3dInitialized(Dictionary<string, Object3dPort> object3dPorts, CancellationToken cancellationToken);

        void OnObject3dAdded(Object3dPort object3DPort, CancellationToken cancellationToken);
        void OnObject3dRemoved(string entityId);
    }
}
