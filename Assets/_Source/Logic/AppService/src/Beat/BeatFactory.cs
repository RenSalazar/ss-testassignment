using Logic.AppService.Context;
using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Event;
using Logic.CoreDomain.Beat;
using System.Threading;
using System.Threading.Tasks;

namespace Logic.AppService.Beat
{
    internal static class BeatFactory
    {
        internal static async Task<IBeatEntity> CreateCubeRoomBeatAndCommitToRepository(
            IContext context,
            CancellationToken cancellationToken)
        {
            ID<IBeatEntity> newBeatId = context.BeatRepository.GetNextId();

            IBeatEntity beat = CoreDomain.Beat.BeatFactory.Create(newBeatId);
            await context.BeatRepository.AddAsync(beat);
            DomainEvents.Fire();
            return beat;
        }
    }
}
