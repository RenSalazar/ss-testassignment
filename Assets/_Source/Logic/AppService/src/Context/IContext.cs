using Logic.CoreDomain.Beat;
using Logic.CoreDomain.Object3d;

namespace Logic.AppService.Context
{
    public interface IContext : System.IDisposable
    {
        IBeatRepository BeatRepository { get; }
        IObject3dRepository Object3dRepository { get; }
    }
}
