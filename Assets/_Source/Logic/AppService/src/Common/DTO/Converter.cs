using System;
using System.Collections.Generic;
using Logic.Common.BaseDomain.Data;
using Logic.Common.DataValues.Beat;
using Logic.CoreDomain.Component;
using GradientDataValue = Logic.Common.DataValues.Gradient.Gradient;
using GradientColorKeyDataValue = Logic.Common.DataValues.Gradient.GradientColorKey;
using ColorDataValue = Logic.Common.DataValues.Gradient.Color;

namespace Logic.AppService.Common.DTO
{
    public static class Converter
    {
        public static IReadOnlyCollection<string> ToStringIds<T>(IReadOnlyCollection<ID<T>> ids)
        {
            List<string> intIds = new List<string>();
            foreach (ID<T> id in ids)
            {
                intIds.Add(id.Serialize());
            }
            return intIds;
        }

        public static string[] ToStringIdArray<T>(IReadOnlyCollection<ID<T>> ids)
        {
            string[] stringIds = new string[ids.Count];
            int index = 0;
            foreach (ID<T> id in ids)
            {
                stringIds[index++] = id.Serialize();
            }
            return stringIds;
        }
        public static string[] ToStringIdArray<T>(IReadOnlyList<ID<T>> ids)
        {
            string[] stringIds = new string[ids.Count];
            int index = 0;
            for (int i = 0; i < ids.Count; ++i)
            {
                stringIds[index++] = ids[i].Serialize();
            }
            return stringIds;
        }

        public static List<ID<T>> ToEntityIdList<T>(string[] ids)
        {
            if (ids == null)
            {
                return new List<ID<T>>();
            }
            List<ID<T>> entityIds = new List<ID<T>>(ids.Length);
            foreach (string intId in ids)
            {
                entityIds.Add(new ID<T>(intId));
            }
            return entityIds;
        }
        public static Title ToTitle(string titleString)
        {
            return new Title(titleString);
        }
        public static Transform ToTransformDTO(ITransform transform)
        {
            return new Transform {
                Position = ToVector3DTO(transform.Position),
                Rotation = ToQuaternionDTO(transform.Rotation),
                Scale = ToVector3DTO(transform.Scale)
            };
        }

        private static Vector3 ToVector3DTO(Logic.Common.DataValues.Common.Vector3 input)
        {
            return new Vector3 {
                X = input.X,
                Y = input.Y,
                Z = input.Z
            };
        }

        private static Quaternion ToQuaternionDTO(Logic.Common.DataValues.Common.Quaternion input)
        {
            return new Quaternion {
                X = input.X,
                Y = input.Y,
                Z = input.Z,
                W = input.W
            };
        }
        internal static ITransform ToTransformEntityComponent(Transform transform)
        {
            return TransformFactory.Create(
                    new Logic.Common.DataValues.Common.Vector3(transform.Position.X, transform.Position.Y, transform.Position.Z),
                    new Logic.Common.DataValues.Common.Quaternion(transform.Rotation.X, transform.Rotation.Y, transform.Rotation.Z, transform.Rotation.W),
                    new Logic.Common.DataValues.Common.Vector3(transform.Scale.X, transform.Scale.Y, transform.Scale.Z
                ));
        }
        internal static System.Tuple<ID<TEntity>, ITransform> ToTransformEntityComponent<TEntity>(string id, Transform transform)
        {
            return new System.Tuple<ID<TEntity>, ITransform>(new ID<TEntity>(id), ToTransformEntityComponent(transform));
        }

        public static DateTime TimeStampToDateTime(long timeStamp)
        {
            return new DateTime(timeStamp, DateTimeKind.Utc);
        }
        public static long DateTimeToTimeStamp(DateTime creationDate)
        {
            long unixTime = creationDate.Ticks;
            return unixTime;
        }

        public static Gradient.Gradient ToDTOGradient(GradientDataValue gradient)
        {
            Gradient.GradientColorKey[] gradientColorKeys = new Gradient.GradientColorKey[gradient.ColorKeys.Length];
            for (int i = 0; i < gradientColorKeys.Length; ++i)
            {
                gradientColorKeys[i] = ToDTOGradientColorKey(gradient.ColorKeys[i]);
            }
            return new Gradient.Gradient(gradientColorKeys);
        }
        public static Gradient.GradientColorKey ToDTOGradientColorKey(GradientColorKeyDataValue gradientColorKey)
        {
            return new Gradient.GradientColorKey(ToDTOColor(gradientColorKey.Color), gradientColorKey.Time);
        }
        public static Gradient.Color ToDTOColor(ColorDataValue color)
        {
            return new Gradient.Color(color.Red, color.Green, color.Blue, color.Alpha);
        }

        public static GradientDataValue ToDomainGradient(Gradient.Gradient gradient)
        {
            GradientColorKeyDataValue[] colorKeys = new GradientColorKeyDataValue[gradient.GradientColorKeys.Length];
            for (int i = 0; i < colorKeys.Length; ++i)
            {
                colorKeys[i] = ToDomainGradientColorKey(gradient.GradientColorKeys[i]);
            }
            return new GradientDataValue(colorKeys);
        }
        public static GradientColorKeyDataValue ToDomainGradientColorKey(Gradient.GradientColorKey gradientColorKey)
        {
            return new GradientColorKeyDataValue(ToDomainColor(gradientColorKey.Color), gradientColorKey.Time);
        }
        public static ColorDataValue ToDomainColor(Gradient.Color color)
        {
            return new ColorDataValue(
                        color.Red,
                        color.Green,
                        color.Blue,
                        color.Alpha);
        }
    }
}
