namespace Logic.AppService.Common.DTO
{
    [System.Serializable]
    public struct Quaternion
    {
        public static readonly Quaternion Identity = new Quaternion(0, 0, 0, 1);

        public float X;
        public float Y;
        public float Z;
        public float W;

        public Quaternion(float x, float y, float z, float w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

    }
}


