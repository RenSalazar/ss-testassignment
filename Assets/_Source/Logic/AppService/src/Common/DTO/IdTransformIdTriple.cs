namespace Logic.AppService.Common.DTO
{
    [System.Serializable]
    public struct IdTransformIdTriple
    {
        public string IdOne;
        public Transform Transform;
        public string IdTwo;

        public IdTransformIdTriple(string idOne, Transform transform, string idTwo)
        {
            IdOne = idOne;
            Transform = transform;
            IdTwo = idTwo;
        }
    }
}
