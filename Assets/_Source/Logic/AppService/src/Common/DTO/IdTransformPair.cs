
namespace Logic.AppService.Common.DTO
{
    [System.Serializable]
    public struct IdTransformPair
    {
        public string Id;
        public Transform Transform;

        public IdTransformPair(string id, Transform transform)
        {
            Id = id;
            Transform = transform;
        }
    }
}
