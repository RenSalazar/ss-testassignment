
namespace Logic.AppService.Common.DTO
{
    [System.Diagnostics.DebuggerDisplay("({X},{Y},{Z})")]
    [System.Serializable]
    public struct Vector3
    {
        public static readonly Vector3 Zero = new Vector3(0, 0, 0);
        public static readonly Vector3 One = new Vector3(1, 1, 1);

        public float X;
        public float Y;
        public float Z;

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 operator *(Vector3 first, float mul)
        {
            return new Vector3(first.X * mul, first.Y * mul, first.Z * mul);
        }
    }
}
