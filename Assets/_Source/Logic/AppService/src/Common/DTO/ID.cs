
namespace Logic.AppService.Common.DTO
{
    public class ID
    {
        public string Id;

        public ID() { }
        public ID(string id)
        {
            Id = id;
        }
    }
}
