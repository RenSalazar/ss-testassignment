namespace Logic.AppService.Common.DTO.Gradient
{
    [System.Serializable]
    public struct GradientColorKey
    {
        public Color Color;
        public float Time;
        public GradientColorKey(Color color, float time)
        {
            Color = color;
            Time = time;
        }
    }
}
