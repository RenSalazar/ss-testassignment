namespace Logic.AppService.Common.DTO.Gradient
{
    [System.Serializable]
    public struct Gradient
    {
        public GradientColorKey[] GradientColorKeys;

        public Gradient(GradientColorKey[] gradientColorKeys)
        {
            GradientColorKeys = gradientColorKeys;
        }
    }
}
