namespace Logic.AppService.Common.DTO.Gradient
{
    [System.Diagnostics.DebuggerDisplay("RGBA({Red}, {Green}, {Blue}, {Alpha})")]
    [System.Serializable]
    public struct Color
    {
        public float Red;
        public float Green;
        public float Blue;
        public float Alpha;

        public Color(float red, float green, float blue, float alpha)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }
    }
}
