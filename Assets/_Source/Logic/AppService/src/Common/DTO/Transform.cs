namespace Logic.AppService.Common.DTO
{
    [System.Serializable]
    public struct Transform
    {
        public static readonly Transform Default =
            new Transform(Vector3.Zero, Quaternion.Identity, Vector3.One);

        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public Transform(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }
    }
}


