using Logic.Common.BaseDomain.Event;

namespace Logic.AppService.Common.EventHandler
{
    internal sealed class CallbackFor<TEvent> : IHandle<TEvent>
        where TEvent : class, IDomainEvent
    {
        private readonly System.Action<TEvent> mAction;

        public CallbackFor(System.Action<TEvent> action)
        {
            mAction = action ?? throw new System.ArgumentNullException(nameof(action));
        }

        public override void HandleAsync(TEvent domainEvent)
        {
            mAction.Invoke(domainEvent);
        }
    }

    internal abstract class Base<TEvent, TListener> : IHandle<TEvent>
        where TEvent : class, IDomainEvent
    {
        protected readonly TListener mListener;

        protected Base(TListener listener)
        {
            mListener = listener;
        }
    }

    internal abstract class Base<TEvent, TListener, TRepository> : Base<TEvent, TListener>
        where TEvent : class, IDomainEvent
    {
        protected readonly TRepository mRepository;

        protected Base(TListener listener, TRepository repository) :
            base(listener)
        {
            mRepository = repository;
        }
    }

    internal abstract class Base<TEvent, TContext, TListener, TRepository> : Base<TEvent, TListener, TRepository>
        where TEvent : class, IDomainEvent
    {
        protected readonly TContext mContext;

        protected Base(TContext context, TListener listener, TRepository repository) :
            base(listener, repository)
        {
            mContext = context;
        }
    }
}
