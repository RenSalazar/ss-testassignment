using Logic.AppService.Beat;
using Logic.Common.BaseDomain.Event;
using Logic.CoreDomain.Beat;
using System.Threading;

namespace Logic.AppService.Creation.Panel
{
    public sealed class Service : System.IDisposable
    {
        private readonly Context.IContext mContext;
        private readonly Context.IContext mLocalContext;
        private readonly IServiceListener mListener;

        private IBeatEntity _beat;

        public Service(
            Context.IContext context,
            Context.IContext localContext,
            IServiceListener listener)
        {
            mContext = context;
            mLocalContext = localContext;
            mListener = listener;
        }
        public void Dispose()
        {
        }

        public async void StartDefaultCreationProcess(CancellationToken cancellationToken)
        {
            _beat = await Beat.BeatFactory.CreateCubeRoomBeatAndCommitToRepository(mLocalContext, cancellationToken);
            BeatPort beatPort = new BeatPort(mLocalContext, _beat);

            mListener.OnDefaultCreationProcessStarted(beatPort, new BeatObjectsPort(mLocalContext));
        }
        public void RequestExitProcess()
        {
            mListener.OnExitDefaultProcess();
        }
        public async void PublishBeatAndExitProcessAsync()
        {
            DomainEvents.Fire();
            await mLocalContext.BeatRepository.CommitAsync(_beat);
            mListener.OnBeatPublished();
        }
    }
}
