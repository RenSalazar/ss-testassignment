
namespace Logic.AppService.Creation.Panel
{
    public interface IServiceListener
    {
        void OnDefaultCreationProcessStarted(Beat.BeatPort beatPort, BeatObjectsPort beatObjectsPort);

        void OnExitDefaultProcess();

        void OnBeatPublished();
    }
}
