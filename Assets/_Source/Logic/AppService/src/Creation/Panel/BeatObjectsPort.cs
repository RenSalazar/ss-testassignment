using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Event;
using Logic.Common.DataValues.Common;
using Logic.Common.DataValues.Gradient;
using Logic.CoreDomain.Component;
using Logic.CoreDomain.Object3d;
using System.Threading;

namespace Logic.AppService.Creation.Panel
{
    public sealed class BeatObjectsPort
    {
        private readonly Context.IContext mContext;

        internal BeatObjectsPort(Context.IContext context)
        {
            mContext = context;
        }

        public async void CreateObject3d(
            Beat.IBeatEntityProvider beatEntityProvider,
            string object3dUrl,
            Common.DTO.Vector3 position,
            Common.DTO.Vector3 scale,
            Common.DTO.Quaternion quaternion,
            Color color,
            CancellationToken cancellationToken)
        {
            var transform = new Common.DTO.Transform(position, quaternion, scale);
            ITransform object3dTransform = Common.DTO.Converter.ToTransformEntityComponent(transform);

            ID<IObject3dEntity> id = mContext.Object3dRepository.GetNextId();
            IObject3dEntity object3DEntity = Object3dFactory.Create(
                id,
                object3dTransform,
                new Url(object3dUrl),
                color);
            await mContext.Object3dRepository.AddAsync(object3DEntity);
            beatEntityProvider.BeatEntity.AddObject3d(id, cancellationToken);
            DomainEvents.Fire();
        }
        public void RemoveObject3d(Beat.IBeatEntityProvider beatEntityProvider, string id)
        {
            beatEntityProvider.BeatEntity.RemoveObject3d(new ID<IObject3dEntity>(id));
            DomainEvents.Fire();
        }
    }
}
