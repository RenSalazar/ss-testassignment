using System.Collections.Generic;

namespace Logic.Common.DataValues.Gradient
{
    public struct Gradient
    {
        public static readonly Gradient WhiteOneColor = new Gradient(
            new GradientColorKey[] { new GradientColorKey(Color.White, 0) });

        public GradientColorKey[] ColorKeys { get; }

        public Gradient(GradientColorKey[] colorKeys)
        {
            ColorKeys = colorKeys;
        }

        public override bool Equals(object obj)
        {
            var comparer = new global::Support.EqualityComparer.ArrayEqualityComparer<GradientColorKey>();
            return obj is Gradient gradient &&
                   comparer.Equals(ColorKeys, gradient.ColorKeys);
        }

        public override int GetHashCode()
        {
            var comparer = new global::Support.EqualityComparer.ArrayEqualityComparer<GradientColorKey>();
            return 447883388 + comparer.GetHashCode(ColorKeys);
        }
    }
}
