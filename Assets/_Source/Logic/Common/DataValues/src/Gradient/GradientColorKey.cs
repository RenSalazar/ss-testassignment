namespace Logic.Common.DataValues.Gradient
{
    public struct GradientColorKey
    {
        public Color Color { get; }
        public float Time { get; }

        public GradientColorKey(Color color, float time)
        {
            Color = color;
            Time = time;
        }

        public override bool Equals(object obj)
        {
            return obj is GradientColorKey key &&
                   Color.Equals(key.Color) &&
                   Time == key.Time;
        }
        public override int GetHashCode()
        {
            int hashCode = -1738997828;
            hashCode = hashCode * -1521134295 + Color.GetHashCode();
            hashCode = hashCode * -1521134295 + Time.GetHashCode();
            return hashCode;
        }
    }
}
