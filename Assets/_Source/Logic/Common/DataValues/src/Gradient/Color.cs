using System;

namespace Logic.Common.DataValues.Gradient
{
    public struct Color : IEquatable<Color>
    {
        public static readonly Color White = new Color(1, 1, 1, 1);

        public float Red { get; }
        public float Green { get; }
        public float Blue { get; }
        public float Alpha { get; }

        public Color(float red, float green, float blue, float alpha)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }

        public Color(float red, float green, float blue) :
            this(red, green, blue, 1f)
        {
        }

        public override int GetHashCode()
        {
            return Red.GetHashCode() ^ (Green.GetHashCode() << 2) ^ (Blue.GetHashCode() >> 2) ^ (Alpha.GetHashCode() >> 1);
        }

        public override bool Equals(object other)
        {
            if (!(other is Color))
                return false;

            return Equals((Color)other);
        }

        public bool Equals(Color other)
        {
            return Red.Equals(other.Red) && Green.Equals(other.Green) && Blue.Equals(other.Blue) && Alpha.Equals(other.Alpha);
        }

        public override string ToString()
        {
            return $"Color {{\"Red\": {Red}, \"Green\": {Green}, \"Blue\": {Blue}, \"Alpha\": {Alpha}}}";
        }
    }
}
