namespace Logic.Common.DataValues.Beat
{
    public struct Text
    {
        private readonly string mValue;

        public Text(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new System.ArgumentException("Text is null or contain only white space(s)", nameof(value));
            }
            mValue = value;
        }

        public string Serialize()
        {
            return mValue;
        }
    }
}


