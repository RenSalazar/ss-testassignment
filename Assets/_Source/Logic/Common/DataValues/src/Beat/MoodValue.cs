
namespace Logic.Common.DataValues.Beat
{
    public struct MoodValue
    {
        public static readonly MoodValue Min = new MoodValue(MIN_VALUE);
        public static readonly MoodValue Max = new MoodValue(MAX_VALUE);

        public const byte MIN_VALUE = 0;
        public const byte MAX_VALUE = 255;

        private readonly byte mValue;

        public MoodValue(byte value)
        {
            if (value < MIN_VALUE || value > MAX_VALUE)//In case the min or max changes
            {
                throw new System.ArgumentOutOfRangeException($"The Mood Value is between {MIN_VALUE} and {MAX_VALUE}, provided: {value}");
            }
            mValue = value;
        }

        public byte Serialize()
        {
            return mValue;
        }

        public override bool Equals(object obj)
        {
            return obj is MoodValue value &&
                   mValue == value.mValue;
        }

        public override int GetHashCode()
        {
            return -952055727 + mValue.GetHashCode();
        }

        public static bool operator ==(MoodValue lhs, MoodValue rhs)
        {
            return lhs.mValue == rhs.mValue;
        }
        public static bool operator !=(MoodValue lhs, MoodValue rhs)
        {
            return lhs.mValue != rhs.mValue;
        }
    }
}
