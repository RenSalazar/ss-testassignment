namespace Logic.Common.DataValues.Beat
{
    public enum EmojiType
    {
        Anger = 0,
        Disgust = 1,
        Joy = 2,
        Sadness = 3,
        Satisfaction = 4,
        Surprise = 5,
        Fear = 6,
        Awe = 7,
        Love = 8,
        Serenity = 9,
        Excited = 10
    }
}
