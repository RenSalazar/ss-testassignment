
namespace Logic.Common.DataValues.Beat
{
    public enum ShapeType
    {
        Polygon,
        Square,
        RoundSquare,
        Triangle,
        Star,
        CustomMesh
    }
}
