namespace Logic.Common.DataValues.Beat
{
    public enum RoomType
    {
        Cube,
        Horizontal,
        Vertical,
        Empty,
        Terrain = 100
    }
}
