using System.Collections.Generic;

namespace Logic.Common.DataValues.Beat
{
    public struct Weather
    {
        private readonly string mUrl;
        private readonly float mIntensity;

        public Weather(string url, float intensity)
        {
            mUrl = url;
            mIntensity = intensity;
        }

        public string SerializeUrl()
        {
            return mUrl;
        }
        public float SerializeIntensity()
        {
            return mIntensity;
        }

        public override bool Equals(object obj)
        {
            return obj is Weather weather &&
                   mUrl == weather.mUrl &&
                   mIntensity == weather.mIntensity;
        }

        public override int GetHashCode()
        {
            var hashCode = 1204663690;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(mUrl);
            hashCode = hashCode * -1521134295 + mIntensity.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Weather lhs, Weather rhs)
        {
            return lhs.mUrl == rhs.mUrl && lhs.mIntensity == rhs.mIntensity;
        }
        public static bool operator !=(Weather lhs, Weather rhs)
        {
            return lhs.mUrl != rhs.mUrl || lhs.mIntensity != rhs.mIntensity;
        }
    }
}
