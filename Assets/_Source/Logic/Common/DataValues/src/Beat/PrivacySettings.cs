namespace Logic.Common.DataValues.Beat
{
    public enum PrivacySettings
    {
        Public,
        Friends,
        Private,
        Draft,
    }
}
