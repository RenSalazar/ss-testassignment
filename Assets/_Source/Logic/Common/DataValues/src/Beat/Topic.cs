namespace Logic.Common.DataValues.Beat
{
    public struct Topic
    {
        private readonly string mTopicText;

        public Topic(string topicText)
        {
            mTopicText = topicText.Replace("#", string.Empty);
        }

        public string Serialize()
        {
            return mTopicText;
        }
    }
}
