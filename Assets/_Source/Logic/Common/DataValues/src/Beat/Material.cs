namespace Logic.Common.DataValues.Beat
{
    public struct Material
    {
        private readonly int mMaterial;

        public Material(int material)
        {
            mMaterial = material;
        }

        public int Serialize()
        {
            return mMaterial;
        }
    }
}
