namespace Logic.Common.DataValues.Beat
{
    public struct Title
    {
        private readonly string mTitleText;

        public Title(string titleText)
        {
            mTitleText = titleText;
        }

        public string Serialize()
        {
            return mTitleText;
        }
    }
}
