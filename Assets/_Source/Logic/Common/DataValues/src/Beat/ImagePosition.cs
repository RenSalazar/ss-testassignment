namespace Logic.Common.DataValues.Beat
{
    public struct ImagePosition
    {
        public float X { get; }
        public float Y { get; }
        public float Z { get; }

        public ImagePosition(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
