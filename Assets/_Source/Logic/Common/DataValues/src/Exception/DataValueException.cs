
namespace Logic.Common.DataValues.Exception
{
    public abstract class DataValueException : System.Exception
    {
        protected DataValueException(string message) :
            base(message)
        {
        }

        protected DataValueException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}
