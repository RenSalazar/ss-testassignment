namespace Logic.Common.DataValues.Exception
{
    public sealed class InvalidFormatException : DataValueException
    {
        public InvalidFormatException(string message) : base(message)
        {
        }
    }
}
