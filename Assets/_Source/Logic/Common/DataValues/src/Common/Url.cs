
namespace Logic.Common.DataValues.Common
{
    public struct Url
    {
        public static readonly Url Invalid = new Url("invalid_url");

        private readonly string mUrl;

        public Url(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new System.ArgumentException(nameof(url));
            }
            mUrl = url;
        }

        public string Serialize()
        {
            return mUrl;
        }

        public static bool operator ==(Url lhs, Url rhs)
        {
            return lhs.mUrl == rhs.mUrl;
        }

        public static bool operator !=(Url lhs, Url rhs)
        {
            return lhs.mUrl != rhs.mUrl;
        }

        public bool Equals(Url other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            if (obj is Url)
            {
                return Equals((Url)obj);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return mUrl.GetHashCode();
        }

        public override string ToString()
        {
            return mUrl;
        }
    }
}
