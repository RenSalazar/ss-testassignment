
namespace Logic.Common.DataValues.Common
{
    public struct Date
    {
        private readonly string mDate;

        public Date(string date)
        {
            mDate = date;
        }

        public string Serialize()
        {
            return mDate;
        }
    }
}
