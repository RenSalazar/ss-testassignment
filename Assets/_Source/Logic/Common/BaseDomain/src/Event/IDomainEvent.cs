using System.Threading;

namespace Logic.Common.BaseDomain.Event
{
    public interface IDomainEvent
    {
        CancellationToken CancellationToken { get; }
    }
}
