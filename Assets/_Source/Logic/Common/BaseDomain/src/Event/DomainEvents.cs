using System.Collections.Generic;

namespace Logic.Common.BaseDomain.Event
{
    public static class DomainEvents
    {
        private sealed class Data
        {
            public readonly Queue<KeyValuePair<System.Type, List<IDomainEvent>>> DomainEvents =
                new Queue<KeyValuePair<System.Type, List<IDomainEvent>>>();
            public readonly Dictionary<System.Type, HashSet<IHandleInternal>> DomainEventHandlers =
                new Dictionary<System.Type, HashSet<IHandleInternal>>();
        }
        [System.ThreadStatic]
        private static Data mData = new Data();

        public static void Add(IDomainEvent domainEvent)
        {
            System.Type eventType = domainEvent.GetType();

            for (int i = 0; i < mData.DomainEvents.Count; ++i)
            {
                KeyValuePair<System.Type, List<IDomainEvent>> pair = mData.DomainEvents.Peek();
                if (pair.Key == eventType)
                {
                    pair.Value.Add(domainEvent);
                    return;
                }
            }
            mData.DomainEvents.Enqueue(
                new KeyValuePair<System.Type, List<IDomainEvent>>(
                    eventType, new List<IDomainEvent> { domainEvent }));
        }

        public static void Subscribe<TEvent>(IHandle<TEvent> eventHandler)
            where TEvent : class, IDomainEvent
        {
            var eventHandlers = AddIfNullAndGetEventHandlers<TEvent>();
            eventHandlers.Add(eventHandler);
        }
        public static void UnSubscribe<TEvent>(IHandle<TEvent> eventHandler)
            where TEvent : class, IDomainEvent
        {
            var eventHandlers = AddIfNullAndGetEventHandlers<TEvent>();
            eventHandlers.Remove(eventHandler);
        }

        public static void Fire()
        {
            while (mData.DomainEvents.Count > 0)
            {
                KeyValuePair<System.Type, List<IDomainEvent>> domainEventPair = mData.DomainEvents.Dequeue();
                if (mData.DomainEventHandlers.TryGetValue(domainEventPair.Key, out HashSet<IHandleInternal> eventHandlers))
                {
                    NotifyAllHandlers(eventHandlers, domainEventPair.Value);
                }
            }
        }

        public static void ClearHandlers()
        {
            mData.DomainEventHandlers.Clear();
        }
        public static void ClearEvents()
        {
            mData.DomainEvents.Clear();
        }

        private static void NotifyAllHandlers(ICollection<IHandleInternal> eventHandlers, ICollection<IDomainEvent> events)
        {
            foreach (IHandleInternal eventHandler in eventHandlers)
            {
                NotifyEventHandler(eventHandler, events);
            }
        }
        private static void NotifyEventHandler(IHandleInternal eventHandler, ICollection<IDomainEvent> events)
        {
            foreach (IDomainEvent domainEventInstance in events)
            {
                eventHandler.HandleInternal(domainEventInstance);
            }
        }
        private static HashSet<IHandleInternal> AddIfNullAndGetEventHandlers<TEventType>()
            where TEventType : IDomainEvent
        {
            System.Type eventType = typeof(TEventType);
            if (!mData.DomainEventHandlers.TryGetValue(eventType, out HashSet<IHandleInternal> handlers))
            {
                handlers = new HashSet<IHandleInternal>();
                mData.DomainEventHandlers.Add(eventType, handlers);
            }
            return handlers;
        }
    }
}
