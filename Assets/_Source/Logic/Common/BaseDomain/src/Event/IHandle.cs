
namespace Logic.Common.BaseDomain.Event
{
    public interface IHandleInternal
    {
        void HandleInternal(IDomainEvent domainEvent);
    }
    public abstract class IHandle<TEvent> : IHandleInternal
        where TEvent : class, IDomainEvent
    {
        public void HandleInternal(IDomainEvent domainEvent)
        {
            HandleAsync(domainEvent as TEvent);
        }

        public abstract void HandleAsync(TEvent domainEvent);
    }
}

