using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Logic.Common.BaseDomain.Repository
{
    public interface IRepository : System.IDisposable
    {
    }
    public interface IRepository<TEntity> : IRepository
        where TEntity : Entity.IBaseEntity<TEntity>
    {
        /// <summary>
        /// Adds new entity to the repository.
        /// It throws an <see cref="Exception.EntityAlreadyInRepository"/> if the entity already exists
        /// </summary>
        /// <param name="entity"></param>
        Task AddAsync(TEntity entity);
        /// <summary>
        /// Removes entity from repository
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns>True if the entity was removed, false if it was not found</returns>
        Task<bool> RemoveAsync(Data.ID<TEntity> entityId);//TODO: add CancellationToken parameter
        /// <summary>
        /// Removes all entities from repository
        /// </summary>
        void Clear();
        /// <summary>
        /// Updates the entity values in repository. The entity should exist.
        /// It throws an <see cref="Exception.EntityNotFound"/> exception if the entity does not exist in repository
        /// </summary>
        /// <param name="entity"></param>
        Task CommitAsync(TEntity entity);
        /// <summary>
        /// Finds entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //TEntity Find(Data.ID<TEntity> id);
        Task<TEntity> FindAsync(Data.ID<TEntity> id, CancellationToken cancellationToken);
        /// <summary>
        /// Finds all unique entities by ids.
        /// Unique means: if the <paramref name="ids"/> array contain repeated ids
        /// the returning entity array will contain only entities with different ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>Unique entities</returns>
        IEnumerable<Task<TEntity>> FindAll(IEnumerable<Data.ID<TEntity>> ids, CancellationToken cancellationToken);
        /// <summary>
        /// Gives new entity id back
        /// </summary>
        /// <returns>New entity id</returns>
        Data.ID<TEntity> GetNextId();
    }
}
