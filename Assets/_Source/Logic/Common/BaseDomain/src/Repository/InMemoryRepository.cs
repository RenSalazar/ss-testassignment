using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Logic.Common.BaseDomain.Repository
{
    public abstract class InMemoryRepository<TEntity> : IRepository<TEntity>
        where TEntity : Entity.IBaseEntity<TEntity>
    {
        private readonly Dictionary<Data.ID<TEntity>, TEntity> mEntities = new Dictionary<Data.ID<TEntity>, TEntity>();

        protected virtual void Dispose(bool disposing) { }
        public void Dispose()
        {
            Dispose(true);
        }

        public Task AddAsync(TEntity entity)
        {
            try
            {
                mEntities.Add(entity.Id, entity);
                return Task.CompletedTask;
            }
            catch (System.ArgumentException ex)
            {
                throw new Exception.EntityAlreadyInRepository(ex);
            }
        }

        public Task<bool> RemoveAsync(Data.ID<TEntity> idToRemove)
        {
            return Task.FromResult(mEntities.Remove(idToRemove));
        }

        public void Clear()
        {
            mEntities.Clear();
        }

        public Task CommitAsync(TEntity entity)
        {
            try
            {
                mEntities[entity.Id] = entity;
                return Task.CompletedTask;
            }
            catch (KeyNotFoundException ex)
            {
                throw new Exception.EntityNotFound(ex, entity.Id.Serialize());
            }
        }

        public Task<TEntity> FindAsync(Data.ID<TEntity> id, CancellationToken cancellationToken)
        {
            try
            {
                return Task.FromResult(mEntities[id]);
            }
            catch (KeyNotFoundException ex)
            {
                throw new Exception.EntityNotFound(ex, id.Serialize());
            }
        }

        public IEnumerable<Task<TEntity>> FindAll(IEnumerable<Data.ID<TEntity>> ids, CancellationToken cancellationToken)
        {
            foreach (Data.ID<TEntity> id in ids.Distinct())
            {
                yield return FindAsync(id, cancellationToken);
            }
        }

        public Data.ID<TEntity> GetNextId()
        {
            int nextId = 0;
            foreach (var key in mEntities.Keys)
            {
                nextId = System.Math.Max(nextId, int.Parse(key.Serialize()) + 1);
            }
            return new Data.ID<TEntity>(nextId.ToString());
        }
        public Task<Data.ID<TEntity>> GetFirstItemIdAsync()
        {
            if (mEntities.Count == 0)
            {
                return Task.FromResult(Data.ID<TEntity>.Invalid);
            }
            return Task.FromResult(mEntities.Values.First().Id);
        }
        public Task<IReadOnlyList<TEntity>> EnumerateAsync(Data.ID<TEntity> startId, int firstItemsCount, CancellationToken cancellationToken)
        {
            if (firstItemsCount <= 0)
            {
                throw new System.ArgumentOutOfRangeException(nameof(firstItemsCount));
            }
            List<TEntity> result = new List<TEntity>(firstItemsCount);
            if (!mEntities.TryGetValue(startId, out TEntity startEntity))
            {
                return Task.FromResult(result as IReadOnlyList<TEntity>);
            }
            int counter = -1;
            foreach (TEntity entity in mEntities.Values)
            {
                if (startEntity.Id == entity.Id)
                {
                    counter = 0;
                }
                if (counter >= 0 && counter <= firstItemsCount)
                {
                    result.Add(entity);
                    counter++;
                }
            }
            return Task.FromResult(result as IReadOnlyList<TEntity>);
        }
    }
}
