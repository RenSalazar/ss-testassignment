using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Logic.Common.BaseDomain.Data;

namespace Logic.Common.BaseDomain.Repository
{
    public abstract class JsonRepository<TEntity> : IRepository<TEntity>
         where TEntity : Entity.IBaseEntity<TEntity>
    {
        private int _nextId;
        protected readonly string mFinalPath;

        protected JsonRepository(string persistentDataPath, string entitiesDirectoryName)
        {
            if (string.IsNullOrWhiteSpace(persistentDataPath))
            {
                throw new System.ArgumentException("Persistent data path can't be null or whitespace", nameof(persistentDataPath));
            }
            if (string.IsNullOrWhiteSpace(entitiesDirectoryName))
            {
                throw new System.ArgumentException("Entity directory name can't be null or whitespace", nameof(entitiesDirectoryName));
            }
            mFinalPath = Path.Combine(persistentDataPath, Path.Combine("Repositories", entitiesDirectoryName));
            Directory.CreateDirectory(mFinalPath);
            _nextId = CalculateNextId();
        }
        protected virtual void Dispose(bool disposing) { }
        public void Dispose()
        {
            Dispose(true);
        }

        public Task AddAsync(TEntity entity)
        {
            SaveEntityToJson(entity);
            return Task.CompletedTask;
        }

        public Task<bool> RemoveAsync(ID<TEntity> beatId)
        {
            string entityFilePath = EntityFilePath(beatId);
            if (!File.Exists(entityFilePath))
            {
                return Task.FromResult(false);
            }
            File.Delete(entityFilePath);
            return Task.FromResult(true);
        }
        public void Clear()
        {
            Directory.Delete(mFinalPath);
            Directory.CreateDirectory(mFinalPath);
        }
        public Task CommitAsync(TEntity entity)
        {
            SaveEntityToJson(entity);
            return Task.CompletedTask;
        }

        public Task<TEntity> FindAsync(ID<TEntity> id, CancellationToken cancellationToken)
        {
            string entityFilePath = EntityFilePath(id);
            if (!File.Exists(entityFilePath))
            {
                throw new Exception.EntityNotFound(id.Serialize(), typeof(TEntity));
            }
            string json = File.ReadAllText(entityFilePath);
            return Task.FromResult(GetEntity(json));
        }
        public IEnumerable<Task<TEntity>> FindAll(IEnumerable<ID<TEntity>> ids, CancellationToken cancellationToken)
        {
            List<Task<TEntity>> tasks = new List<Task<TEntity>>();
            foreach (ID<TEntity> id in ids.Distinct())
            {
                var task = FindAsync(id, cancellationToken);
                tasks.Add(task);
            }
            return tasks;
        }
        public ID<TEntity> GetNextId()
        {
            return new ID<TEntity>((_nextId++).ToString());
        }
        public Task<ID<TEntity>> GetFirstItemIdAsync()
        {
            string[] files = Directory.GetFiles(mFinalPath, "*", SearchOption.TopDirectoryOnly);
            if (files.Length > 0)
            {
                string fileName = Path.GetFileName(files[0]);
                return Task.FromResult(new ID<TEntity>(fileName));
            }
            return Task.FromResult(ID<TEntity>.Invalid);
        }
        public Task<IReadOnlyList<TEntity>> EnumerateAsync(ID<TEntity> startId, int firstItemsCount, CancellationToken cancellationToken)
        {
            if (firstItemsCount <= 0)
            {
                throw new System.ArgumentOutOfRangeException(nameof(firstItemsCount));
            }
            string[] files = Directory.GetFiles(mFinalPath, "*", SearchOption.TopDirectoryOnly);
            List<TEntity> result = new List<TEntity>(firstItemsCount);
            int startIdIndex = System.Array.IndexOf(files, EntityFilePath(startId));
            if (startIdIndex == -1)
            {
                return Task.FromResult(result as IReadOnlyList<TEntity>);
            }
            int lastItemIndex = startIdIndex + firstItemsCount;
            for (int i = startIdIndex; i < files.Length && i < lastItemIndex; ++i)
            {
                result.Add(FindAsync(new ID<TEntity>(files[i]), cancellationToken).Result);
            }
            return Task.FromResult(result as IReadOnlyList<TEntity>);
        }

        private void SaveEntityToJson(TEntity entity)
        {
            string json = GetJson(entity);
            File.WriteAllText(EntityFilePath(entity.Id), json);
        }
        private string EntityFilePath(ID<TEntity> entityId)
        {
            return Path.Combine(mFinalPath, entityId.Serialize());
        }
        private int CalculateNextId()
        {
            string[] files = Directory.GetFiles(mFinalPath, "*", SearchOption.TopDirectoryOnly);
            int maxId = -1;
            foreach (string file in files)
            {
                string fileName = Path.GetFileName(file);
                maxId = System.Math.Max(maxId, int.Parse(fileName));
            }
            maxId++;
            return maxId;
        }

        protected abstract TEntity GetEntity(string json);
        protected abstract string GetJson(TEntity entity);
    }
}
