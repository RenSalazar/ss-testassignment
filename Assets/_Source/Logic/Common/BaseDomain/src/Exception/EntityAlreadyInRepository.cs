
namespace Logic.Common.BaseDomain.Exception
{
    public sealed class EntityAlreadyInRepository : DomainException
    {
        public EntityAlreadyInRepository(string entityId)
            : base("Entity id: " + entityId)
        {
        }
        public EntityAlreadyInRepository(System.Exception inner) :
            base(inner.Message, inner)
        {
        }
    }
}
