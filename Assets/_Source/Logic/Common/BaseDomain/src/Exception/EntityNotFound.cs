
namespace Logic.Common.BaseDomain.Exception
{
    public sealed class EntityNotFound : DomainException
    {
        public EntityNotFound(string entityId, System.Type entityType)
            : base($"Entity id: \"{entityId}\", entity type: \"{entityType}\"")
        {
        }

        public EntityNotFound(System.Collections.Generic.KeyNotFoundException inner, string entityId) :
            base(inner.Message + " Entity id: " + entityId, inner)
        {
        }
    }
}
