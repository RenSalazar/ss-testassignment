
namespace Logic.Common.BaseDomain.Exception
{
    public abstract class DomainException : System.Exception
    {
        protected DomainException(string message) :
            base(message)
        {
        }

        protected DomainException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}
