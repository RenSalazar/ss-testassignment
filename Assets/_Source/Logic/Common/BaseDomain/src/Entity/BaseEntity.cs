
namespace Logic.Common.BaseDomain.Entity
{
    [System.Diagnostics.DebuggerDisplay("Id = {Id}")]
    public abstract class Entity<TEntity> : IBaseEntity<TEntity>
    {
        public Data.ID<TEntity> Id { get; private set; }

        protected Entity(Data.ID<TEntity> id)
        {
            Id = id;
        }
    }
}
