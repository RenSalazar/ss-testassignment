
namespace Logic.Common.BaseDomain.Entity
{
    public interface IBaseEntity<TEntity>
    {
        Data.ID<TEntity> Id { get; }
    }
}
