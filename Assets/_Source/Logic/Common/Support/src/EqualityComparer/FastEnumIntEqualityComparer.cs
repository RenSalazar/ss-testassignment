using System.Collections.Generic;

namespace Support.EqualityComparer
{
    public struct FastEnumIntEqualityComparer<TEnum> : IEqualityComparer<TEnum>
        where TEnum : struct
    {
        public bool Equals(TEnum firstEnum, TEnum secondEnum)
        {
            return ToInt(firstEnum) == ToInt(secondEnum);
        }
        public int GetHashCode(TEnum firstEnum)
        {
            return ToInt(firstEnum);
        }
        private int ToInt(TEnum en)
        {
            return EnumInt32ToInt.Convert(en);
        }
    }
}
