using System.Collections.Generic;

namespace Support.EqualityComparer
{
    public struct ArrayEqualityComparer<T> : IEqualityComparer<T[]>
    {
        // You could make this a per-instance field with a constructor parameter
        private static readonly EqualityComparer<T> mElementComparer
            = EqualityComparer<T>.Default;

        public bool Equals(T[] first, T[] second)
        {
            if (first == second)
            {
                return true;
            }
            if (first == null || second == null)
            {
                return false;
            }
            if (first.Length != second.Length)
            {
                return false;
            }
            for (int i = 0; i < first.Length; ++i)
            {
                if (!mElementComparer.Equals(first[i], second[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public int GetHashCode(T[] array)
        {
            unchecked
            {
                if (array == null)
                {
                    return 0;
                }
                int hash = 17;
                for (int i = 0; i < array.Length; ++i)
                {
                    T element = array[i];
                    hash = hash * 31 + mElementComparer.GetHashCode(element);
                }
                return hash;
            }
        }
    }
}
