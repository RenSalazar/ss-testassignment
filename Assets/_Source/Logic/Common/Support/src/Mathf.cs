
namespace Logic.Common.Support
{
    public static class Mathf
    {
        public const float PI = (float)System.Math.PI;

        public static int Clamp(int value, int min, int max)
        {
            if (value < min)
            {
                value = min;
            }
            else if (value > max)
            {
                value = max;
            }
            return value;
        }

        public static float Clamp01(float value)
        {
            if (value < 0f)
            {
                value = 0f;
            }
            else if (value > 1f)
            {
                value = 1f;
            }
            return value;
        }

        public static float Lerp(float minAmount, float maxAmount, float time)
        {
            return (minAmount * (1f - time)) + (time * maxAmount);
        }

        public static float Sqrt(float v) { return (float)System.Math.Sqrt(v); }
        public static float Sin(float f) { return (float)System.Math.Sin(f); }
        public static float Cos(float f) { return (float)System.Math.Cos(f); }
    }
}
