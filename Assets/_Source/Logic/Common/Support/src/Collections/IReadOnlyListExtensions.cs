using System.Collections.Generic;

namespace Support.Collections
{
    public static class IReadOnlyListExtensions
    {
        public static T Find<T>(this IReadOnlyList<T> self, System.Predicate<T> match)
        {
            for (int i = 0; i < self.Count; ++i)
            {
                T elem = self[i];
                if (match(elem))
                {
                    return elem;
                }
            }
            return default(T);
        }
    }
}
