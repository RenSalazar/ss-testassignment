using UnityEngine;

namespace Presentation.Utility
{
    internal sealed class ScreenOrientation : MonoBehaviour
    {
        public event System.Action<UnityEngine.ScreenOrientation> ScreenOrientationChanged;
        private const float RANGE = 100f;

        private UnityEngine.ScreenOrientation _currentScreenOrientation;
        private float _currentWidth;
        private float _currentHeight;

        private void OnEnable()
        {
            _currentScreenOrientation = Screen.orientation;
#if UNITY_EDITOR
            _currentScreenOrientation = GetEditorScreenOrientation();
#endif
            _currentWidth = Screen.width;
            _currentHeight = Screen.height;
        }

        internal Vector3 GetRelativePosition(Vector2 position)
        {
            return new Vector3(position.x * RANGE / _currentWidth, position.y * RANGE / _currentHeight);
        }

        private void Update()
        {
            HandleScreenOrientation();
        }

        private void HandleScreenOrientation()
        {
#if UNITY_EDITOR
            var newOrientation = GetEditorScreenOrientation();
            if (newOrientation != _currentScreenOrientation)
            {
                OnScreenOrientationChanged(newOrientation);
            }
#else
            if (_currentScreenOrientation != Screen.orientation)
            {
                OnScreenOrientationChanged(Screen.orientation);
            }
#endif
        }

        private void OnScreenOrientationChanged(UnityEngine.ScreenOrientation orientation)
        {
            _currentWidth = Screen.width;
            _currentHeight = Screen.height;
            _currentScreenOrientation = orientation;
            ScreenOrientationChanged?.Invoke(_currentScreenOrientation);
        }

#if UNITY_EDITOR
        private UnityEngine.ScreenOrientation GetEditorScreenOrientation()
        {
            return Screen.width > Screen.height ? UnityEngine.ScreenOrientation.Landscape : UnityEngine.ScreenOrientation.Portrait;
        }
#endif
    }
}
