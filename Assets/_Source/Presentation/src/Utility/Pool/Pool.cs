using UnityEngine;

namespace Presentation.Utility.Pool
{
    internal static class Pool
    {
        public static T Spawn<T>(T prefab, Transform parent = null, bool worldPositionStays = false)
            where T : Component
        {
            return Lean.Pool.LeanPool.Spawn(prefab, parent, worldPositionStays);
        }

        public static void Despawn(GameObject clone)
        {
            Lean.Pool.LeanPool.Despawn(clone);
        }
    }
}
