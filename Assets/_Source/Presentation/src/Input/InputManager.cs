using UnityEngine;

using UnityInput = UnityEngine.Input;

namespace Presentation.Input
{
    internal enum FlickDirection
    {
        HorizontalLeft,
        HorizontalRight,
        VerticalUp,
        VerticalDown
    }

    internal delegate void ActionOnePosition(
        Vector2 absolutePositionInPixels,
        Vector2 relativePositionInPercent);
    internal delegate void ActionTwoPositions(
        Vector2 absolutePositionInPixelsOne,
        Vector2 absolutePositionInPixelsTwo,
        Vector2 relativePositionInPercentOne,
        Vector2 relativePositionInPercentTwo);

    internal sealed class InputManager : MonoBehaviour
    {
        [SerializeField] private Utility.ScreenOrientation _screenOrientation;
        [Header("Tap and DoubleTap")]
        [SerializeField] private float _moveToleranceForTrueTapInPercent = 2f;
        [SerializeField] private float _timeToleranceForDoubleTapInSeconds = 0.3f;
        [Header("Flick (Fast Swipe)")]
        [SerializeField] private float _minMoveDistanceForHorizontalFlickActionInPercent = 25f;
        [SerializeField] private float _minMoveDistanceForVerticalFlickActionInPercent = 10f;
        [SerializeField] private float _maxTimeForFlickActionInSeconds = 0.3f;
        [Header("Three touches tap")]
        [SerializeField] private float _timeToleranceBetweenTouchBeganPhasesForThreeTapEventInSeconds = 0.15f;
        [Header("Simulate in editor touches")]
        [SerializeField] private float _scrollSensibilityForTwoTouchesSimulation = 0.15f;

        public event ActionOnePosition Tap;
        public event ActionOnePosition DoubleTap;
        public event ActionOnePosition TapAndHold;
        public event System.Action<FlickDirection> Flick;
        public event System.Action ThreeTouchesTap;

        public event ActionOnePosition FirstTouchBegan;
        public event ActionOnePosition FirstTouchMoved;
        public event ActionOnePosition FirstTouchStationary;
        public event ActionOnePosition FirstTouchEnded;

        public event ActionTwoPositions TwoTouchesBegan;
        public event ActionTwoPositions TwoTouchesMoved;
        public event ActionTwoPositions TwoTouchesStationary;
        public event ActionTwoPositions TwoTouchesEnded;

        private Vector2 _prevAbsolutePositionInPixels;
        private Vector3 _mousePositionOffsetForSecondTouch;
        private Vector2 _tapBeganPositionInPercent = Vector2.negativeInfinity;
        private Vector2 _firstTouchBeganPositionInPercent = Vector2.negativeInfinity;

        private float _unscaledTimeAtTouchBegan;
        private Coroutine _tapAndDoubleTapAndTapAndHoldCoroutine;
        private Coroutine _threeTapEventCoroutine;

        private void Update()
        {
#if UNITY_EDITOR
            HandleMouseInput();
            SimulateThreeTouchesTap();
#else
            HandleTouchInput();
            HandleThreeTouchesTap();
#endif
        }

        private void HandleTouchInput()
        {
            HandleOneTouch();
            HandleTwoTouches();
            CancelOneOrDoubleTapIfMoreThatOneFingersTouchTheScreen();
        }

        private void HandleOneTouch()
        {
            if (UnityInput.touchCount != 1)
            {
                return;
            }
            var touch = UnityInput.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                _unscaledTimeAtTouchBegan = Time.unscaledTime;
                Vector3 currentPos = touch.position;
                _firstTouchBeganPositionInPercent = _screenOrientation.GetRelativePosition(currentPos);
                if (_tapAndDoubleTapAndTapAndHoldCoroutine == null)
                {
                    _tapAndDoubleTapAndTapAndHoldCoroutine = StartCoroutine(
                        CallTapOrDoubleTapOrTapAndHoldEventsIfNecessary(
                            currentPos,
                            _firstTouchBeganPositionInPercent));
                }
                CallEvent(currentPos, _firstTouchBeganPositionInPercent, FirstTouchBegan);
            }
            if (touch.phase == TouchPhase.Moved)
            {
                Vector3 currentPos = touch.position;
                Vector2 currentRelativePos = _screenOrientation.GetRelativePosition(currentPos);
                if (_tapAndDoubleTapAndTapAndHoldCoroutine != null && !PositionIsStationaryToFirstTouchBeganWithTolerance(currentRelativePos))
                {
                    StopCoroutine(_tapAndDoubleTapAndTapAndHoldCoroutine);
                    _tapAndDoubleTapAndTapAndHoldCoroutine = null;
                }
                CallEvent(currentPos, currentRelativePos, FirstTouchMoved);
            }
            if (touch.phase == TouchPhase.Stationary)
            {
                Vector3 currentPos = touch.position;
                CallEvent(currentPos, _screenOrientation.GetRelativePosition(currentPos), FirstTouchStationary);
            }
            if (touch.phase == TouchPhase.Ended)
            {
                Vector3 currentAbsolutePos = touch.position;
                Vector2 currentRelativePos = _screenOrientation.GetRelativePosition(currentAbsolutePos);
                CallFlickEventIfNecessary(currentAbsolutePos, currentRelativePos);
                ResetTouchPositionsAndTime();
                CallEvent(currentAbsolutePos, currentRelativePos, FirstTouchEnded);
            }
        }

        private void HandleTwoTouches()
        {
            if (UnityInput.touchCount != 2)
            {
                return;
            }
            var touchOne = UnityInput.GetTouch(0);
            var touchTwo = UnityInput.GetTouch(1);
            if (touchTwo.phase == TouchPhase.Began)
            {
                CallEvent(touchOne.position, touchTwo.position, TwoTouchesBegan);
            }
            if (touchOne.phase == TouchPhase.Moved || touchTwo.phase == TouchPhase.Moved)
            {
                CallEvent(touchOne.position, touchTwo.position, TwoTouchesMoved);
            }
            if (touchOne.phase == TouchPhase.Stationary && touchTwo.phase == TouchPhase.Stationary)
            {
                CallEvent(touchOne.position, touchTwo.position, TwoTouchesStationary);
            }
            if (touchOne.phase == TouchPhase.Ended)
            {
                CallEvent(touchTwo.position, touchOne.position, TwoTouchesEnded);
            }
            if (touchTwo.phase == TouchPhase.Ended)
            {
                CallEvent(touchOne.position, touchTwo.position, TwoTouchesEnded);
            }
        }

        private void HandleMouseInput()
        {
            SimulateOneTouch();
            SimulateTwoTouches();
        }

        private void SimulateOneTouch()
        {
            if (UnityInput.GetMouseButtonDown(0))
            {
                _unscaledTimeAtTouchBegan = Time.unscaledTime;
                Vector3 currentPos = UnityInput.mousePosition;
                _firstTouchBeganPositionInPercent = _screenOrientation.GetRelativePosition(currentPos);
                if (_tapAndDoubleTapAndTapAndHoldCoroutine == null)
                {
                    _tapAndDoubleTapAndTapAndHoldCoroutine = StartCoroutine(
                        CallTapOrDoubleTapOrTapAndHoldEventsIfNecessary(
                            currentPos,
                            _firstTouchBeganPositionInPercent));
                }
                CallEvent(currentPos, _firstTouchBeganPositionInPercent, FirstTouchBegan);
            }
            if (UnityInput.GetMouseButton(0) && (Vector2)UnityInput.mousePosition != _prevAbsolutePositionInPixels)
            {
                Vector3 currentPos = UnityInput.mousePosition;
                Vector2 currentRelativePos = _screenOrientation.GetRelativePosition(currentPos);
                if (_tapAndDoubleTapAndTapAndHoldCoroutine != null && !PositionIsStationaryToFirstTouchBeganWithTolerance(currentRelativePos))
                {
                    StopCoroutine(_tapAndDoubleTapAndTapAndHoldCoroutine);
                    _tapAndDoubleTapAndTapAndHoldCoroutine = null;
                }
                CallEvent(currentPos, currentRelativePos, FirstTouchMoved);
            }
            else if (UnityInput.GetMouseButton(0) && PositionIsStationaryToFirstTouchBeganWithTolerance(UnityInput.mousePosition))
            {
                Vector3 currentPos = UnityInput.mousePosition;
                CallEvent(currentPos, _screenOrientation.GetRelativePosition(currentPos), FirstTouchStationary);
            }
            if (UnityInput.GetMouseButtonUp(0))
            {
                Vector3 currentAbsolutePos = UnityInput.mousePosition;
                Vector2 currentRelativePos = _screenOrientation.GetRelativePosition(currentAbsolutePos);
                CallFlickEventIfNecessary(currentAbsolutePos, currentRelativePos);
                ResetTouchPositionsAndTime();
                CallEvent(currentAbsolutePos, currentRelativePos, FirstTouchEnded);
            }
        }

        private float _timeWhenTwoTouchesBeganForEditorSimulation;
        private void SimulateTwoTouches()
        {
            if (UnityInput.GetMouseButtonDown(1) || UnityInput.GetMouseButtonDown(4))
            {
                _timeWhenTwoTouchesBeganForEditorSimulation = Time.time;
                _mousePositionOffsetForSecondTouch = CreateCirclePointsBasedOnTime(_timeWhenTwoTouchesBeganForEditorSimulation);
                SimulateCallEventTwoTouches(UnityInput.mousePosition, TwoTouchesBegan);
            }
            if (UnityInput.GetMouseButton(1) &&
                ((Vector2)UnityInput.mousePosition != _prevAbsolutePositionInPixels ||
                UnityInput.mouseScrollDelta.y != 0f))
            {
                _mousePositionOffsetForSecondTouch -=
                    UnityInput.mouseScrollDelta.y *
                    CreateCirclePointsBasedOnTime(_timeWhenTwoTouchesBeganForEditorSimulation) *
                    _scrollSensibilityForTwoTouchesSimulation;
                SimulateCallEventTwoTouches(UnityInput.mousePosition, TwoTouchesMoved);
            }
            else if (UnityInput.GetMouseButton(1) && PositionIsStationaryToFirstTouchBeganWithTolerance(UnityInput.mousePosition))
            {
                SimulateCallEventTwoTouches(UnityInput.mousePosition, TwoTouchesStationary);
            }
            if (UnityInput.GetMouseButtonUp(1) || UnityInput.GetMouseButtonUp(4))
            {
                SimulateCallEventTwoTouches(UnityInput.mousePosition, TwoTouchesEnded);
            }
            if (UnityInput.GetMouseButton(4))
            {
                _mousePositionOffsetForSecondTouch = CreateCirclePointsBasedOnTime(Time.time);
                SimulateCallEventTwoTouches(UnityInput.mousePosition, TwoTouchesMoved);
            }
        }
        private void SimulateCallEventTwoTouches(Vector3 position, ActionTwoPositions toCall)
        {
            CallEvent(
                    position + _mousePositionOffsetForSecondTouch,
                    position - _mousePositionOffsetForSecondTouch,
                    toCall);
        }

        private void CallEvent(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent, ActionOnePosition toCall)
        {
            toCall?.Invoke(absolutePositionInPixels, relativePositionInPercent);
            UpdatePrevFirstTouchPosition(absolutePositionInPixels);
        }
        private void CallEvent(Vector2 absolutePositionInPixelsOne, Vector2 absolutePositionInPixelsTwo, ActionTwoPositions toCall)
        {
            toCall?.Invoke(
                absolutePositionInPixelsOne,
                absolutePositionInPixelsTwo,
                _screenOrientation.GetRelativePosition(absolutePositionInPixelsOne),
                _screenOrientation.GetRelativePosition(absolutePositionInPixelsTwo));
            UpdatePrevFirstTouchPosition(absolutePositionInPixelsOne);
        }
        private void UpdatePrevFirstTouchPosition(Vector2 absolutePositionInPixels)
        {
            _prevAbsolutePositionInPixels = absolutePositionInPixels;
        }

        private System.Collections.IEnumerator CallTapOrDoubleTapOrTapAndHoldEventsIfNecessary(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            yield return null;
            _tapBeganPositionInPercent = relativePositionInPercent;
            bool fingerWasReleased = false;
            float count = 0f;
            while (count < _timeToleranceForDoubleTapInSeconds)
            {
                if (TouchInBeganPhase())
                {
                    GetCurrentTouchPosition(out Vector2 absolutePositionInPixelsSecondTouch, out Vector2 relativePositionInPercentSecondTouch);
                    if (PositionIsStationaryToTapBeganWithTolerance(relativePositionInPercentSecondTouch))
                    {
                        DoubleTap?.Invoke(absolutePositionInPixels, relativePositionInPercent);
                        _tapAndDoubleTapAndTapAndHoldCoroutine = null;
                        yield break;
                    }
                }
                if (TouchInEndPhase())
                {
                    fingerWasReleased = true;
                }
                count += Time.deltaTime;
                yield return null;
            }
            _tapAndDoubleTapAndTapAndHoldCoroutine = null;
            if (!fingerWasReleased)
            {
                TapAndHold?.Invoke(absolutePositionInPixels, relativePositionInPercent);
            }
            else
            {
                Tap?.Invoke(absolutePositionInPixels, relativePositionInPercent);
            }
        }

        private bool TouchInBeganPhase()
        {
#if UNITY_EDITOR
            return UnityInput.GetMouseButtonDown(0);
#else
            return UnityInput.touchCount == 1 && UnityInput.GetTouch(0).phase == TouchPhase.Began;
#endif
        }
        private bool TouchInEndPhase()
        {
#if UNITY_EDITOR
            return UnityInput.GetMouseButtonUp(0);
#else
            return UnityInput.touchCount == 1 && UnityInput.GetTouch(0).phase == TouchPhase.Ended;
#endif
        }

        private void GetCurrentTouchPosition(out Vector2 absolutePositionInPixels, out Vector2 relativePositionInPercent)
        {
#if UNITY_EDITOR
            absolutePositionInPixels = UnityInput.mousePosition;
#else
            absolutePositionInPixels = UnityInput.GetTouch(0).position;
#endif
            relativePositionInPercent = _screenOrientation.GetRelativePosition(absolutePositionInPixels);
        }

        private void CallFlickEventIfNecessary(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            float xDiff = relativePositionInPercent.x - _firstTouchBeganPositionInPercent.x;
            float yDiff = relativePositionInPercent.y - _firstTouchBeganPositionInPercent.y;
            float xAbsDiff = Mathf.Abs(xDiff);
            float yAbsDiff = Mathf.Abs(yDiff);
            if ((xAbsDiff > _minMoveDistanceForHorizontalFlickActionInPercent ||
                (yAbsDiff > _minMoveDistanceForVerticalFlickActionInPercent)) &&
                Time.unscaledTime - _unscaledTimeAtTouchBegan < _maxTimeForFlickActionInSeconds)
            {
                FlickDirection flickDirection = CalculateFlickDirection(xDiff, yDiff, xAbsDiff, yAbsDiff);
                Flick?.Invoke(flickDirection);
            }
        }

        private void ResetTouchPositionsAndTime()
        {
            _firstTouchBeganPositionInPercent = Vector2.negativeInfinity;
            _unscaledTimeAtTouchBegan = 0;
        }

        private FlickDirection CalculateFlickDirection(float xDiff, float yDiff, float xAbsDiff, float yAbsDiff)
        {
            if (xAbsDiff > yAbsDiff)
            {
                if (xDiff > 0)
                    return FlickDirection.HorizontalRight;
                else
                    return FlickDirection.HorizontalLeft;
            }
            else
            {
                if (yDiff > 0)
                    return FlickDirection.VerticalUp;
                else
                    return FlickDirection.VerticalDown;
            }
        }

        private bool PositionIsStationaryToFirstTouchBeganWithTolerance(Vector2 currentRelativePos)
        {
            return (currentRelativePos - _firstTouchBeganPositionInPercent).sqrMagnitude < _moveToleranceForTrueTapInPercent;
        }
        private bool PositionIsStationaryToTapBeganWithTolerance(Vector2 currentRelativePos)
        {
            return (currentRelativePos - _tapBeganPositionInPercent).sqrMagnitude < _moveToleranceForTrueTapInPercent;
        }
        private void CancelOneOrDoubleTapIfMoreThatOneFingersTouchTheScreen()
        {
            if (UnityInput.touchCount > 1 && _tapAndDoubleTapAndTapAndHoldCoroutine != null)
            {
                StopCoroutine(_tapAndDoubleTapAndTapAndHoldCoroutine);
                _tapAndDoubleTapAndTapAndHoldCoroutine = null;
            }
        }
        private void SimulateThreeTouchesTap()
        {
            if (UnityInput.GetMouseButtonUp(3))
            {
                ThreeTouchesTap?.Invoke();
            }
        }
        private void HandleThreeTouchesTap()
        {
            if (_threeTapEventCoroutine == null &&
                UnityInput.touchCount >= 1 &&
                    (UnityInput.GetTouch(0).phase == TouchPhase.Began ||
                    (UnityInput.touchCount >= 2 && UnityInput.GetTouch(1).phase == TouchPhase.Began) ||
                    (UnityInput.touchCount == 3 && UnityInput.GetTouch(2).phase == TouchPhase.Began)))
            {
                _threeTapEventCoroutine = StartCoroutine(CallThreeTapEventIfNecessary());
            }
            if (UnityInput.touchCount > 3 && _threeTapEventCoroutine != null)
            {
                StopCoroutine(_threeTapEventCoroutine);
                _threeTapEventCoroutine = null;
            }
        }
        private System.Collections.IEnumerator CallThreeTapEventIfNecessary()
        {
            float time = 0;
            while (time < _timeToleranceBetweenTouchBeganPhasesForThreeTapEventInSeconds)
            {
                if (UnityInput.touchCount == 3 &&
                    (UnityInput.GetTouch(0).phase == TouchPhase.Ended ||
                    UnityInput.GetTouch(1).phase == TouchPhase.Ended ||
                    UnityInput.GetTouch(2).phase == TouchPhase.Ended))
                {
                    ThreeTouchesTap?.Invoke();
                    _threeTapEventCoroutine = null;
                    yield break;
                }
                time += Time.deltaTime;
                yield return null;
            }
            _threeTapEventCoroutine = null;
        }

        private static Vector3 CreateCirclePointsBasedOnTime(float time)
        {
            return new Vector3(Screen.width * Mathf.Cos(time), Screen.height * Mathf.Sin(time));
        }
    }
}
