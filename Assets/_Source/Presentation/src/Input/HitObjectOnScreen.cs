using UnityEngine;

namespace Presentation.Input
{
    public interface IRaycastDraggableScreenSpace
    {
        void OnDragBegin(Vector2 absolutePositionInPixels, Vector3 hitPoint);
        void OnDrag(Vector2 absolutePositionInPixels);
        void OnDragEnd(Vector2 absolutePositionInPixels);
    }

    public class HitObjectOnScreen : MonoBehaviour
    {
        public static event System.Action RaycastHitMissInCurrentLayer;

        [SerializeField] private InputManager _inputManager;
        [SerializeField] private LayerMask _layer;
        [SerializeField] private float _distanceToCheck;
        [SerializeField] private Camera _camera;
        [SerializeField] private Camera _uiCamera;

        public Camera Camera { get { return _camera; } set { _camera = value; } }
        public LayerMask CurrentLayerForInput { get { return _layer; } }

        private IRaycastDraggableScreenSpace _currentlyDraggedObjectScreenSpace;
        private bool _enabled;

        private RaycastHit[] _hitResults;

        private void Awake()
        {
            if (_camera == null)
            {
                Debug.LogError($"No camera reference set on {name} component on {gameObject.name}");
            }
            InputBlockAboveUI.SetUiCamera(_uiCamera);
            _hitResults = new RaycastHit[3];
        }

        private void OnEnable()
        {
            _inputManager.Tap += OnTap;
            _inputManager.DoubleTap += OnDoubleTap;

            _inputManager.FirstTouchBegan += OnDragBegin;
            _inputManager.FirstTouchMoved += OnDrag;
            _inputManager.FirstTouchEnded += OnDragEnd;

            _enabled = true;
        }

        private void OnDisable()
        {
            _inputManager.Tap -= OnTap;
            _inputManager.DoubleTap -= OnDoubleTap;

            _inputManager.FirstTouchBegan -= OnDragBegin;
            _inputManager.FirstTouchMoved -= OnDrag;
            _inputManager.FirstTouchEnded -= OnDragEnd;

            _enabled = false;
        }

        internal void Disable()
        {
            enabled = false;
        }
        internal void Enable()
        {
            enabled = true;
        }
        internal void SetInputLayer(LayerMask layer)
        {
            _layer = layer;
        }

        private void OnTap(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            if (!_enabled || InputBlockAboveUI.IsOverUi(absolutePositionInPixels))
            {
                return;
            }
            if (_camera == null)
                return;

            Vector2 coordinates = GetTheTapCoordinatesDependingFromRenderTexture(absolutePositionInPixels);
            Ray ray = _camera.ScreenPointToRay(coordinates);
#if UNITY_EDITOR
            Debug.DrawRay(
                ray.GetPoint(0),
                ray.direction * _distanceToCheck,
                Color.green,
                2f);
#endif
            int hitsCount = Physics.RaycastNonAlloc(ray, _hitResults, _distanceToCheck, _layer);
            if (hitsCount > 0)
            {
                for (int i = 0; i < hitsCount; ++i)
                {
                    RaycastHit hit = _hitResults[i];
                    // check for clickables
                    IRaycastHit[] raycastHitComponents = hit.collider.transform.GetComponents<IRaycastHit>();
                    foreach (IRaycastHit component in raycastHitComponents)
                    {
                        component.OnRaycastHit();
                    }
                }
            }
            else
            {
                RaycastHitMissInCurrentLayer?.Invoke();
            }
        }
        private void OnDoubleTap(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            if (!_enabled || InputBlockAboveUI.IsOverUi(absolutePositionInPixels))
            {
                return;
            }
            if (_camera == null)
                return;

            Vector2 coordinates = GetTheTapCoordinatesDependingFromRenderTexture(absolutePositionInPixels);
            Ray ray = _camera.ScreenPointToRay(coordinates);
#if UNITY_EDITOR
            Debug.DrawRay(
                ray.GetPoint(0),
                ray.direction * _distanceToCheck,
                Color.yellow,
                2f);
#endif
            if (Physics.Raycast(ray, out RaycastHit hit, _distanceToCheck, _layer))
            {
                // check for clickables
                IRaycastDoubleHit[] raycastHitComponents = hit.collider.transform.GetComponents<IRaycastDoubleHit>();
                foreach (IRaycastDoubleHit component in raycastHitComponents)
                {
                    component.OnRaycastDoubleHit();
                }
            }
        }

        private void OnDragBegin(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            if (!_enabled || _camera == null || InputBlockAboveUI.IsOverUi(absolutePositionInPixels))
            {
                return;
            }
            if (Physics.Raycast(_camera.ScreenPointToRay(absolutePositionInPixels), out RaycastHit hit, _distanceToCheck, _layer))
            {
                IRaycastDraggableScreenSpace draggableScreenSpace = hit.collider.transform.GetComponent<IRaycastDraggableScreenSpace>();
                if (draggableScreenSpace == null && hit.collider.GetType() == typeof(CapsuleCollider))//TODO: refactor it
                {
                    draggableScreenSpace = hit.collider.transform.parent.GetComponent<IRaycastDraggableScreenSpace>();
                }
                if (draggableScreenSpace != null)
                {
                    _currentlyDraggedObjectScreenSpace = draggableScreenSpace;
                    _currentlyDraggedObjectScreenSpace.OnDragBegin(absolutePositionInPixels, hit.point);
                }
            }
        }
        private void OnDrag(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            if (!_enabled || InputBlockAboveUI.IsOverUi(absolutePositionInPixels))
            {
                return;
            }
            _currentlyDraggedObjectScreenSpace?.OnDrag(absolutePositionInPixels);
        }
        private void OnDragEnd(Vector2 absolutePositionInPixels, Vector2 relativePositionInPercent)
        {
            _currentlyDraggedObjectScreenSpace?.OnDragEnd(absolutePositionInPixels);
            _currentlyDraggedObjectScreenSpace = null;
        }

        private Vector2 GetTheTapCoordinatesDependingFromRenderTexture(Vector2 absolutePositionInPixels)
        {
            Vector2 coordinates = absolutePositionInPixels;
            if (_camera.targetTexture != null)
            {
                float texWidth = _camera.targetTexture.width;
                float texHeight = _camera.targetTexture.height;

                coordinates = new Vector2(
                    absolutePositionInPixels.x / Screen.width * texWidth,
                    absolutePositionInPixels.y / Screen.height * texHeight);
            }
            return coordinates;
        }
    }
}
