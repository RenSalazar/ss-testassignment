using UnityEngine;

namespace Presentation.Input
{
    internal static class InputBlockAboveUI
    {
        private static System.Collections.Generic.HashSet<RectTransform> sUiBlockers = new System.Collections.Generic.HashSet<RectTransform>();
        private static Camera sUiCamera;

        public static void AddUiBlock(RectTransform rect) => sUiBlockers.Add(rect);
        public static void RemoveUiBlock(RectTransform rect) => sUiBlockers.Remove(rect);
        public static bool IsOverUi(Vector2 point)
        {
            foreach (RectTransform rect in sUiBlockers)
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(rect, point, sUiCamera))
                {
                    return true;
                }
            }
            return false;
        }
        public static void SetUiCamera(UnityEngine.Camera camera) => sUiCamera = camera;
    }
}
