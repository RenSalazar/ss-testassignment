﻿namespace Presentation.Input
{
    public interface IRaycastDoubleHit
    {
        void OnRaycastDoubleHit();
    }
}
