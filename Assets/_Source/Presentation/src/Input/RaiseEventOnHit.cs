using UnityEngine;

namespace Presentation.Input
{
    public class RaiseEventOnHit : MonoBehaviour, IRaycastHit
    {
        public System.Action ClickEvent;

        public void OnRaycastHit()
        {
            ClickEvent?.Invoke();
        }
    }
}
