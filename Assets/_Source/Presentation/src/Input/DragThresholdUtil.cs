using UnityEngine;
using UnityEngine.EventSystems;

namespace Presentation.Input
{
    internal sealed class DragThresholdUtil : MonoBehaviour
    {
        private void Start()
        {
            int defaultValue = EventSystem.current.pixelDragThreshold;
            float dpi = Screen.dpi;
            Debug.Log("Current screen dpi is: " + dpi);
            EventSystem.current.pixelDragThreshold = Mathf.Max(defaultValue, (int)(defaultValue * dpi / 160f));
        }
    }
}
