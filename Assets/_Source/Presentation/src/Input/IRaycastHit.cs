﻿namespace Presentation.Input
{
    public interface IRaycastHit
    {
        void OnRaycastHit();
    }
}
