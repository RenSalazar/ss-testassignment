using UnityEngine;

namespace Presentation.Input
{
    internal sealed class DragObjectScreenSpace : MonoBehaviour, IRaycastDraggableScreenSpace
    {
        public System.Action<Vector2, Vector3> DragBeginEvent;
        public System.Action<Vector2> DragEvent;
        public System.Action<Vector2> DragEndEvent;

        public void OnDragBegin(Vector2 position, Vector3 hitPoint)
        {
            DragBeginEvent?.Invoke(position, hitPoint);
        }
        public void OnDrag(Vector2 position)
        {
            DragEvent?.Invoke(position);
        }
        public void OnDragEnd(Vector2 position)
        {
            DragEndEvent?.Invoke(position);
        }
    }
}

