using UnityEngine;

namespace Presentation.Input
{
    public class RaiseEventOnDoubleHit : MonoBehaviour, IRaycastDoubleHit
    {
        public System.Action DoubleClickEvent;

        public void OnRaycastDoubleHit()
        {
            DoubleClickEvent?.Invoke();
        }
    }
}
