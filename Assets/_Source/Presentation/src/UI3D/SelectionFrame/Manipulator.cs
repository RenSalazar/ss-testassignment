using Presentation.Input;
using UnityEngine;

[RequireComponent(typeof(DragObjectScreenSpace))]
internal sealed class Manipulator : MonoBehaviour
{
    private DragObjectScreenSpace _dragObjectScreenSpace;
    
    private void OnEnable()
    {
        _dragObjectScreenSpace = GetComponent<DragObjectScreenSpace>();
        _dragObjectScreenSpace.DragBeginEvent += Activate;
        _dragObjectScreenSpace.DragEndEvent += Deactivate;
    }
    private void OnDisable()
    {
        _dragObjectScreenSpace.DragBeginEvent -= Activate;
        _dragObjectScreenSpace.DragEndEvent -= Deactivate;
    }

    private void Activate(Vector2 fingerScreenPosition, Vector3 hitPoint)
    {
    }
    private void Deactivate(Vector2 fingerScreenPosition)
    {
    }
    
    internal void Show(bool show)
    {
        gameObject.SetActive(show);
    }
}
