using System;
using Presentation.Entity.Common;
using Presentation.Beat.Pool;
using Presentation.Input;
using UnityEngine;

namespace Presentation.UI3D.SelectionFrame
{
    internal sealed class Object3dSelectionFrame : MonoBehaviour, IDisposable
    {
        [Flags]
        private enum ManipulatorType
        {
            MoveX = 1,
            MoveY = 2,
            MoveZ = 4,
            Move = MoveX | MoveY | MoveZ,
            RingX = 8,
            RingY = 16,
            RingZ = 32,
            Ring = RingX | RingY | RingZ,
        }
        public event Action<ISelectable> ObjectSelected;
        public event Action ObjectBeforeDeselected;
        public event Action Moved;
        public event Action Rotated;
        public event Action Scaled;
        public event Action<ISelectable> Deleted;
        public event Action<ISelectable> ColorChanged;

        [Header("Camera")]
        [SerializeField] private Transform _forwardFacingRoot;

        [Header("Rotation / Scale")]
        [SerializeField] private RingManipulationHandlesGroup _ringManipulationHandlesGroup;
        [SerializeField, Range(0, 90)] private int _rotationStep = 15;
        [SerializeField] private DragObjectScreenSpace _yRingHandle;
        [SerializeField] private Manipulator _yRingHandleManipulator;
        [SerializeField] private DragObjectScreenSpace _zRingHandle;
        [SerializeField] private Manipulator _zRingHandleManipulator;
        [SerializeField] private DragObjectScreenSpace _xRingHandle;
        [SerializeField] private Manipulator _xRingHandleManipulator;
        [SerializeField, Range(0f, 1f)] private float _scaleStep = .1f;
        [SerializeField, Min(0)] private int _pixelsToStartScale = 30;

        [Header("Position")]
        [SerializeField] private DragObjectScreenSpace _planeYHandle;
        [SerializeField] private DragObjectScreenSpace _planeZHandle;
        [SerializeField] private DragObjectScreenSpace _planeXHandle;
        [SerializeField] private Transform _moveManipulationHandlesGroup;
        [SerializeField] private DragObjectScreenSpace _xHandle;
        [SerializeField] private Manipulator _xHandleManipulator;
        [SerializeField] private DragObjectScreenSpace _yHandle;
        [SerializeField] private Manipulator _yHandleManipulator;
        [SerializeField] private DragObjectScreenSpace _zHandle;
        [SerializeField] private Manipulator _zHandleManipulator;

        [Header("Delete button")]
        [SerializeField] private RaiseEventOnHit _deleteButton;
        [SerializeField] private Transform _deleteButtonOffset;

        [Header("Color Change button")]
        [SerializeField] private RaiseEventOnHit _colorChangeButton;
        [SerializeField] private ColorPool _colorPool;

        private ISelectable _selectedObject;
        private Entity.Beat.Beat _beat;

        private float _startScaleOffset;
        private Vector3 _scaleForUndo;
        private bool _didScale;

        private bool _didRotate;

        private bool _didMove;
        private Vector3 _grabOffset;

        private Vector3 _grabPosition;
        private Vector3 _grabScale;
        private Vector3 _toVectorFromGrabPositionToObjectPosition;

        private Transform _transform;

        private int _colorPoolCurrentIndex;

        internal void Init()
        {
            _transform = transform;
            _colorPoolCurrentIndex = -1;
            gameObject.SetActive(false);
        }
        public void Dispose()
        {
            Destroy(gameObject);
        }
        private void LateUpdate()
        {
            if (_selectedObject == null)
                return;
            _transform.position = _selectedObject.Transform.position;
            (_forwardFacingRoot ? _forwardFacingRoot : _transform).rotation = GetRotationForForwardFacingRoot();
        }

        private void OnEnable()
        {
            Input.HitObjectOnScreen.RaycastHitMissInCurrentLayer += DeselectObject;

            _deleteButton.ClickEvent += OnDeleteButtonClick;
            _colorChangeButton.ClickEvent += OnColorChangeButtonClick;

            _xHandle.DragBeginEvent += OnMoveStartX;
            _xHandle.DragEvent += OnMoveX;
            _xHandle.DragEndEvent += OnMoveEndX;
            _yHandle.DragBeginEvent += OnMoveStartY;
            _yHandle.DragEvent += OnMoveY;
            _yHandle.DragEndEvent += OnMoveEndY;
            _zHandle.DragBeginEvent += OnMoveStartZ;
            _zHandle.DragEvent += OnMoveZ;
            _zHandle.DragEndEvent += OnMoveEndZ;
            _planeYHandle.DragBeginEvent += OnMovePlaneStart;
            _planeYHandle.DragEvent += OnMovePlaneY;
            _planeYHandle.DragEndEvent += OnMovePlaneEnd;
            _planeZHandle.DragBeginEvent += OnMovePlaneStart;
            _planeZHandle.DragEvent += OnMovePlaneZ;
            _planeZHandle.DragEndEvent += OnMovePlaneEnd;
            _planeXHandle.DragBeginEvent += OnMovePlaneStart;
            _planeXHandle.DragEvent += OnMovePlaneX;
            _planeXHandle.DragEndEvent += OnMovePlaneEnd;

            _yRingHandle.DragBeginEvent += OnRingStartY;
            _yRingHandle.DragEvent += OnRingY;
            _yRingHandle.DragEndEvent += OnRingEndY;
            _zRingHandle.DragBeginEvent += OnRingStartZ;
            _zRingHandle.DragEvent += OnRingZ;
            _zRingHandle.DragEndEvent += OnRingEndZ;
            _xRingHandle.DragBeginEvent += OnRingStartX;
            _xRingHandle.DragEvent += OnRingX;
            _xRingHandle.DragEndEvent += OnRingEndX;
        }

        private void OnDisable()
        {
            Input.HitObjectOnScreen.RaycastHitMissInCurrentLayer -= DeselectObject;

            _deleteButton.ClickEvent -= OnDeleteButtonClick;
            _colorChangeButton.ClickEvent -= OnColorChangeButtonClick;

            _xHandle.DragBeginEvent -= OnMoveStartX;
            _xHandle.DragEvent -= OnMoveX;
            _xHandle.DragEndEvent -= OnMoveEndX;
            _yHandle.DragBeginEvent -= OnMoveStartY;
            _yHandle.DragEvent -= OnMoveY;
            _yHandle.DragEndEvent -= OnMoveEndY;
            _zHandle.DragBeginEvent -= OnMoveStartZ;
            _zHandle.DragEvent -= OnMoveZ;
            _zHandle.DragEndEvent -= OnMoveEndZ;
            _planeYHandle.DragBeginEvent -= OnMovePlaneStart;
            _planeYHandle.DragEvent -= OnMovePlaneY;
            _planeYHandle.DragEndEvent -= OnMovePlaneEnd;
            _planeZHandle.DragBeginEvent -= OnMovePlaneStart;
            _planeZHandle.DragEvent -= OnMovePlaneZ;
            _planeZHandle.DragEndEvent -= OnMovePlaneEnd;
            _planeXHandle.DragBeginEvent -= OnMovePlaneStart;
            _planeXHandle.DragEvent -= OnMovePlaneX;
            _planeXHandle.DragEndEvent -= OnMovePlaneEnd;

            _yRingHandle.DragBeginEvent -= OnRingStartY;
            _yRingHandle.DragEvent -= OnRingY;
            _yRingHandle.DragEndEvent -= OnRingEndY;
            _zRingHandle.DragBeginEvent -= OnRingStartZ;
            _zRingHandle.DragEvent -= OnRingZ;
            _zRingHandle.DragEndEvent -= OnRingEndZ;
            _xRingHandle.DragBeginEvent -= OnRingStartX;
            _xRingHandle.DragEvent -= OnRingX;
            _xRingHandle.DragEndEvent -= OnRingEndX;
        }

        internal void WorkWith(Entity.Beat.Beat beat)
        {
            _beat = beat;
            //_beatBounds = _beat.GetBounds();
            AutoSize[] autoSizers = GetComponentsInChildren<AutoSize>();
            foreach (AutoSize autoSize in autoSizers)
            {
                autoSize.CameraTransform = _beat.BeatCamera;
            }
            _ringManipulationHandlesGroup.CameraTransform = _beat.BeatCamera;
        }
        internal void DeselectObject()
        {
            if (_selectedObject == null)
            {
                return;
            }
            ObjectBeforeDeselected?.Invoke();
            _selectedObject = null;
            if (gameObject != null)
            {
                gameObject.SetActive(false);
            }
            ObjectSelected?.Invoke(null);
        }
        internal ISelectable GetSelectedObject()
        {
            return _selectedObject;
        }
        internal void DuplicateSelectedObject()
        {
            Debug.Log("DuplicateSelectedObject");
        }

        internal void SelectObject(ISelectable selectedWallObject)
        {
            if (selectedWallObject == null)
            {
                throw new ArgumentNullException(nameof(selectedWallObject));
            }
            _selectedObject = selectedWallObject;

            gameObject.SetActive(true);
            _transform.parent = _selectedObject.Transform.parent;
            _transform.position = _selectedObject.Transform.position;
            (_forwardFacingRoot ? _forwardFacingRoot : _transform).rotation = GetRotationForForwardFacingRoot();

            ObjectSelected?.Invoke(_selectedObject);
        }

        private void ControllerActivated(ManipulatorType type)
        {
            if ((type & ManipulatorType.Move) > 0)
            {
                _ringManipulationHandlesGroup.gameObject.SetActive(false);
                _xHandleManipulator.Show(type == ManipulatorType.MoveX);
                _yHandleManipulator.Show(type == ManipulatorType.MoveY);
                _zHandleManipulator.Show(type == ManipulatorType.MoveZ);
            }
            else if ((type & ManipulatorType.Ring) > 0)
            {
                _moveManipulationHandlesGroup.gameObject.SetActive(false);
                _xRingHandleManipulator.Show(type.HasFlag(ManipulatorType.RingX));
                _yRingHandleManipulator.Show(type.HasFlag(ManipulatorType.RingY));
                _zRingHandleManipulator.Show(type.HasFlag(ManipulatorType.RingZ));
            }
            _deleteButton.gameObject.SetActive(false);
            _colorChangeButton.gameObject.SetActive(false);
        }
        private void ControllerDeactivated(ManipulatorType type)
        {
            if ((type & ManipulatorType.Move) > 0)
            {
                _ringManipulationHandlesGroup.gameObject.SetActive(true);
                _xHandleManipulator.Show(true);
                _yHandleManipulator.Show(true);
                _zHandleManipulator.Show(true);
            }
            else if ((type & ManipulatorType.Ring) > 0)
            {
                _moveManipulationHandlesGroup.gameObject.SetActive(true);
                _xRingHandleManipulator.Show(true);
                _yRingHandleManipulator.Show(true);
                _zRingHandleManipulator.Show(true);
            }
            _deleteButton.gameObject.SetActive(true);
            _colorChangeButton.gameObject.SetActive(true);
        }

        private void OnMovePlaneStart(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.Ring);
        }
        private void OnMovePlaneEnd(Vector2 fingerScreenSpacePosition)
        {
            OnMoveEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.Ring);
        }
        private void OnMoveStartX(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.MoveX);
        }
        private void OnMoveStartY(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.MoveY);
        }
        private void OnMoveStartZ(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.MoveZ);
        }
        private void OnRingStartX(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.RingX);
        }
        private void OnRingStartY(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.RingY);
        }
        private void OnRingStartZ(Vector2 fingerScreenSpacePosition, Vector3 hitPoint)
        {
            OnManipulatorStart(fingerScreenSpacePosition, hitPoint);
            ControllerActivated(ManipulatorType.RingZ);
        }
        private void OnMoveEndX(Vector2 fingerScreenSpacePosition)
        {
            OnMoveEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.MoveX);
        }
        private void OnMoveEndY(Vector2 fingerScreenSpacePosition)
        {
            OnMoveEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.MoveY);
        }
        private void OnMoveEndZ(Vector2 fingerScreenSpacePosition)
        {
            OnMoveEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.MoveZ);
        }
        private void OnRingEndX(Vector2 fingerScreenSpacePosition)
        {
            OnRotationOrScaleEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.RingX);
        }
        private void OnRingEndY(Vector2 fingerScreenSpacePosition)
        {
            OnRotationOrScaleEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.RingY);
        }
        private void OnRingEndZ(Vector2 fingerScreenSpacePosition)
        {
            OnRotationOrScaleEnd();
            OnManipulatorEnd(fingerScreenSpacePosition);
            ControllerDeactivated(ManipulatorType.RingZ);
        }
        private void OnManipulatorStart(Vector2 fingerPosition, Vector3 hitPoint)
        {
            _grabOffset = _selectedObject.Transform.position - hitPoint;
            _grabPosition = hitPoint;
            _grabScale = _selectedObject.Transform.localScale;

            _scaleForUndo = _selectedObject.Transform.localScale;
            _toVectorFromGrabPositionToObjectPosition = _grabPosition - _selectedObject.Transform.position;

            _didMove = false;
            _didRotate = false;
            _didScale = false;
        }
        private void OnManipulatorEnd(Vector2 fingerScreenSpacePosition)
        {
            if (_selectedObject == null)
                return;

            if (_didMove)
            {
                _didMove = false;
            }
            if (_didScale)
            {
                ScaleRingHandles(1f);
                _didScale = false;
                _didRotate = false;
            }
            if (_didRotate)
            {
                _didRotate = false;
            }
        }
        private void OnMovePlaneY(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.up, _grabPosition);
            MoveOnPlane(plane, fingerPosition);
        }
        private void OnMovePlaneZ(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.forward, _grabPosition);
            MoveOnPlane(plane, fingerPosition);
        }
        private void OnMovePlaneX(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.left, _grabPosition);
            MoveOnPlane(plane, fingerPosition);
        }
        private void MoveOnPlane(Plane plane, Vector2 fingerPosition)
        {
            Ray ray = _beat.BeatCamera.GetComponent<Camera>().ScreenPointToRay(fingerPosition);
            plane.Raycast(ray, out float enter);
            Vector3 worldPos = ray.GetPoint(enter) + _grabOffset;
            worldPos = TransformHelper.Snap10(worldPos);
            _selectedObject.Transform.position = TransformHelper.Snap10(worldPos);
            _didMove = true;
        }
        private void OnMoveX(Vector2 fingerPosition)
        {
            MoveAxis(Vector3.right, fingerPosition);
        }
        private void OnMoveY(Vector2 fingerPosition)
        {
            MoveAxis(Vector3.up, fingerPosition);
        }
        private void OnMoveZ(Vector2 fingerPosition)
        {
            MoveAxis(Vector3.forward, fingerPosition);
        }
        private void MoveAxis(Vector3 axis, Vector2 fingerPosition)
        {
            Ray ray = _beat.BeatCamera.GetComponent<Camera>().ScreenPointToRay(fingerPosition);
            // get the right cross axis to prevent projection jumps
            Vector3 forward = _beat.BeatCamera.forward;
            Vector3 crossAxis = Vector3.Cross(Vector3.Cross(axis, forward), axis);
            // if the cross is zero, means edge case where camera is ON axis directly, then use camera axis as normal for plane
            Vector3 target;
            if (Math.Abs(crossAxis.magnitude) < .001f)
            {
                Plane plane = new Plane(forward, _grabPosition);
                plane.Raycast(ray, out float enter);
                Vector3 worldPos = ray.GetPoint(enter);
                Vector3 offsetVec = worldPos - _grabPosition;
                float offset = offsetVec.magnitude;
                offset *= offsetVec.y > 0f ? 1f : -1f;
                target = _grabPosition + _grabOffset + (offset * axis);
            }
            else
            {
                Plane plane = new Plane(crossAxis, _grabPosition);
                plane.Raycast(ray, out float enter);
                Vector3 worldPos = ray.GetPoint(enter) + _grabOffset;
                Vector3 offset = worldPos - _selectedObject.Transform.position;
                offset = Vector3.Project(offset, axis);
                target = _selectedObject.Transform.position + offset;
            }

            _selectedObject.Transform.position = TransformHelper.Snap10(target);

            _didMove = true;
        }
        private void OnRingY(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.up, _grabPosition);
            ManipulateRingOnPlane(plane, fingerPosition);
        }
        private void OnRingZ(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.forward, _grabPosition);
            ManipulateRingOnPlane(plane, fingerPosition);
        }
        private void OnRingX(Vector2 fingerPosition)
        {
            Plane plane = new Plane(Vector3.left, _grabPosition);
            ManipulateRingOnPlane(plane, fingerPosition);
        }
        private void ManipulateRingOnPlane(Plane plane, Vector2 fingerPosition)
        {
            // if already in rotation then continue
            if (_didRotate)
            {
                TryToRotateOnPlane(plane, fingerPosition);
                return;
            }
            // if already in scale then continue
            if (_didScale)
            {
                TryToResizeOnPlane(plane, fingerPosition);
                return;
            }
            // if none then first try to scale because scale is harder?
            if (!TryToResizeOnPlane(plane, fingerPosition))
            {
                TryToRotateOnPlane(plane, fingerPosition);
            }
        }

        private bool TryToRotateOnPlane(Plane plane, Vector2 fingerPosition)
        {
            Ray ray = _beat.BeatCamera.GetComponent<Camera>().ScreenPointToRay(fingerPosition);
            plane.Raycast(ray, out float enter);
            Vector3 worldPos = ray.GetPoint(enter);
            Vector3 currentVector = worldPos - _selectedObject.Transform.position;
            Vector3 grabVector = worldPos - _grabPosition;
            float deltaAngle = Vector3.SignedAngle(_toVectorFromGrabPositionToObjectPosition, currentVector, plane.normal);
            int deltaStep = Mathf.RoundToInt(deltaAngle / _rotationStep);
            if (deltaStep == 0)
            {
                return false;
            }
            _selectedObject.Transform.Rotate(plane.normal, deltaStep * _rotationStep, Space.World);
            _toVectorFromGrabPositionToObjectPosition = currentVector;

            _didRotate = true;
            return true;
        }

        private bool TryToResizeOnPlane(Plane plane, Vector2 screenPos)
        {
            Vector3 pos = _selectedObject.Transform.position;
            Camera beatCamera = _beat.BeatCamera.GetComponent<Camera>();
            Vector2 objectScreenPos = beatCamera.WorldToScreenPoint(pos);
            Vector2 grabScreenPos = beatCamera.WorldToScreenPoint(_grabPosition);

            Vector2 grabVector = objectScreenPos - grabScreenPos;
            Vector2 dragVector = objectScreenPos - screenPos;

            float offsetToGrab = (screenPos - objectScreenPos).magnitude - (grabScreenPos - objectScreenPos).magnitude;
            if (Mathf.Abs(offsetToGrab) <= _pixelsToStartScale)
            {
                _startScaleOffset = offsetToGrab;
                if (_didScale)
                {
                    ScaleRingHandles(1f);
                }

                return _didScale;
            }

            // if crossing the middle then were still in scaling but dont do anything
            if (Vector2.Dot(grabVector, dragVector) < 0f)
            {
                _didScale = true;
                return true;
            }

            float grabRadius = grabVector.magnitude - _pixelsToStartScale;
            float dragRadius = dragVector.magnitude - _pixelsToStartScale;

            float offsetDragRadius = dragRadius - _startScaleOffset;

            float scale = Mathf.Max(0f, offsetDragRadius / grabRadius);


            scale *= scale;

            scale = Mathf.Clamp(scale, 0f, 2f);

            ScaleRingHandles(scale);

            Vector3 maxScale = _grabScale * ((_selectedObject.MaxScale * 1.73205f) / _grabScale.magnitude);
            Vector3 minScale = _grabScale * ((_selectedObject.MinScale * 1.73205f) / _grabScale.magnitude);

            Vector3 targetScale = scale > 1f ?
                Vector3.Lerp(_grabScale, maxScale, scale - 1f) :
                Vector3.Lerp(minScale, _grabScale, scale);

            targetScale = TransformHelper.Snap(targetScale, _scaleStep);

            targetScale.x = Mathf.Clamp(targetScale.x, _selectedObject.MinScale, _selectedObject.MaxScale);
            targetScale.y = Mathf.Clamp(targetScale.y, _selectedObject.MinScale, _selectedObject.MaxScale);
            targetScale.z = Mathf.Clamp(targetScale.z, _selectedObject.MinScale, _selectedObject.MaxScale);

            _selectedObject.Transform.localScale = targetScale;


            _didScale = true;
            return true;
        }

        private void ScaleRingHandles(float scale) => _ringManipulationHandlesGroup.SetScale(scale);

        private void ResizeAxis(Vector3 axis, Vector2 fingerPosition)
        {
            Ray ray = _beat.BeatCamera.GetComponent<Camera>().ScreenPointToRay(fingerPosition);
            Plane plane = new Plane(_beat.BeatCamera.forward, _grabPosition);
            plane.Raycast(ray, out float enter);
            Vector3 worldPos = ray.GetPoint(enter);

            Vector3 offset = worldPos - _selectedObject.Transform.position;
            offset = Vector3.Project(offset, axis);
            Vector3 target = _selectedObject.Transform.position + offset;

            Vector3 grabOffset = _grabPosition - _selectedObject.Transform.position;
            grabOffset = Vector3.Project(grabOffset, axis);
            Vector3 grabProjected = _selectedObject.Transform.position + grabOffset;

            Vector3 localTarget = _selectedObject.Transform.InverseTransformPoint(target);
            Vector3 localGrabProjected = _selectedObject.Transform.InverseTransformPoint(grabProjected);

            Vector3 delta = localTarget - localGrabProjected;
            delta.Scale(_selectedObject.Transform.localScale);
            //Debug.Log($"{delta.x}, {delta.y}, {delta.z}");
            Vector3 targetScale = _scaleForUndo + delta;

            //Debug.DrawLine(_beat.BeatCamera.Transform.position, localGrabProjected, Color.cyan, 2);
            //Debug.DrawLine(_beat.BeatCamera.Transform.position, localTarget, Color.magenta, 2);

            //Debug.DrawLine(_beat.BeatCamera.Transform.position, grabProjected, Color.red, 2);
            //Debug.DrawLine(_beat.BeatCamera.Transform.position, target, Color.blue, 2);

            targetScale.x = Mathf.Clamp(targetScale.x, _selectedObject.MinScale, _selectedObject.MaxScale);
            targetScale.y = Mathf.Clamp(targetScale.y, _selectedObject.MinScale, _selectedObject.MaxScale);
            targetScale.z = Mathf.Clamp(targetScale.z, _selectedObject.MinScale, _selectedObject.MaxScale);

            _selectedObject.Transform.localScale = targetScale;//TransformHelper.Snap(targetScale);

            _didScale = true;
        }
        private void OnDeleteButtonClick()
        {
            Deleted?.Invoke(_selectedObject);
            DeselectObject();
        }
        /// <summary>
        /// Renelie Salazar's special method for the tech exam.
        /// Callback method for when the Color Change feature is to be invoked.
        /// </summary>
        private void OnColorChangeButtonClick()
        {
            if (_selectedObject == null)
            {
                return;
            }

            _colorPoolCurrentIndex =
                ((_colorPoolCurrentIndex + 1) >= _colorPool.pool.Count) ? 0
                : (_colorPoolCurrentIndex + 1);

            _selectedObject.Renderer.material.color = _colorPool.pool[_colorPoolCurrentIndex];

            ColorChanged?.Invoke(_selectedObject);
        }
        private Quaternion GetRotationForForwardFacingRoot()
        {
            return Quaternion.LookRotation(-_beat.BeatCamera.forward, _beat.BeatCamera.up);
        }
        private void OnMoveEnd()
        {
            if (_didMove)
            {
                //TODO: add undo
                Moved?.Invoke();
            }
        }
        private void OnRotationOrScaleEnd()
        {
            if (_didRotate)
            {
                //TODO: add undo
                Rotated?.Invoke();
            }
            if (_didScale)
            {
                //TODO: add undo
                Scaled?.Invoke();
            }
        }
    }
}

