using UnityEngine;

namespace Presentation.UI3D.SelectionFrame
{
    internal sealed class AutoSize : MonoBehaviour
    {
        private const int CAMERA_FIELD_OF_VIEW = 60;
        public Vector3 _fixedSize = Vector3.one;
        public Transform CameraTransform;


        private void Update()
        {
            CameraTransform = CameraTransform == null ? Camera.current.transform : CameraTransform;
            Transform tr = transform;
            Vector3 position = tr.position;
            Vector3 camPos = CameraTransform.transform.position;
            float distance = (camPos - position).magnitude;
            Vector3 size = _fixedSize * .001f * (distance * CAMERA_FIELD_OF_VIEW);
            float mag = tr.parent.worldToLocalMatrix.MultiplyVector(size).magnitude;
            size = size.normalized * mag;
            //size is always positive so no flips
            // size.x = Mathf.Abs(size.x);
            // size.y = Mathf.Abs(size.y);
            // size.z = Mathf.Abs(size.z);
            tr.localScale = size;
        }
    }
}
