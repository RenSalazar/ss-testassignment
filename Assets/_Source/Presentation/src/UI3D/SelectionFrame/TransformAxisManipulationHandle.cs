using UnityEngine;

namespace Presentation.UI3D.SelectionFrame
{
    public class TransformAxisManipulationHandle : MonoBehaviour
    {
        private Entity.Beat.Beat _beat;

        [SerializeField] private Vector3 _axis;
        [SerializeField] private Vector3 _axisB;
        [SerializeField] private bool _use2Axis;
        [SerializeField] private bool _keepQuadrant;

        private void OnEnable()
        {
            _beat = GetComponentInParent<Entity.Beat.Beat>();
        }

        private void Update()
        {
            if (_beat == null)
            {
                _beat = GetComponentInParent<Entity.Beat.Beat>();
            }
            if (_beat == null)
            {
                return;
            }
            float angle = 0;
            float angleB = 0;
            if (Vector3.Dot(_beat.BeatCamera.forward, _axis) > 0)
            {
                angle = 180;
            }
            if (_use2Axis && Vector3.Dot(_beat.BeatCamera.forward, _axisB) > 0)
            {
                angleB = 180;
            }
            Quaternion rotation = Quaternion.AngleAxis(angle, _axisB);
            if (_use2Axis)
            {
                rotation *= Quaternion.AngleAxis(angleB, _axis);
            }

            transform.localRotation = rotation;
        }
    }
}
