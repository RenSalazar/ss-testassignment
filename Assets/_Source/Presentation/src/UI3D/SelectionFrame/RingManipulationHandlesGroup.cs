using System;
using UnityEngine;

namespace Presentation.UI3D.SelectionFrame
{
    public class RingManipulationHandlesGroup : MonoBehaviour
    {
        private MaterialPropertyBlock _materialPropertyBlock;
        private float _ringScale = 1f;
        private Renderer[] _renderers;
        
        [SerializeField] private GameObject _xAxisObject;
        [SerializeField] private GameObject _yAxisObject;
        [SerializeField] private GameObject _zAxisObject;
        
        private static readonly int mPush = Shader.PropertyToID("_Push");

        internal Transform CameraTransform { get; set; }

        private void OnEnable()
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
            _renderers = GetComponentsInChildren<Renderer>(true);
        }

        private void Update()
        {
            if (CameraTransform == null)
            {
                return;
            }
            Vector3 camForward = CameraTransform.forward;
            camForward = new Vector3(Mathf.Abs(camForward.x), Mathf.Abs(camForward.y), Mathf.Abs(camForward.z));
            _xAxisObject.SetActive(camForward.x > camForward.y && camForward.x > camForward.z);
            _yAxisObject.SetActive(!_xAxisObject.activeSelf && camForward.y > camForward.z);
            _zAxisObject.SetActive(!_xAxisObject.activeSelf && !_yAxisObject.activeSelf);
        }

        internal void SetScale(float scale)
        {
            _materialPropertyBlock.SetFloat(mPush, scale-1f);
            //rings
            foreach (Renderer r in _renderers)
            {
                r.SetPropertyBlock(_materialPropertyBlock);
            }
        }
    }
}
