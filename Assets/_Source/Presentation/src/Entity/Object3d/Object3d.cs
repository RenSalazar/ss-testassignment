using System;
using System.Threading;
using Logic.AppService.Object3d;
using Presentation.Entity.Common;
using UnityEngine;

namespace Presentation.Entity.Object3d
{
    internal sealed class Object3d : MonoBehaviour,
        IDisposable,
        IObject3dPortListener,
        ISelectable
    {
        public event ClickDelegate Click;
        public event ClickDelegate DoubleClick;

        public string Id { get { return _object3dPort.Id; } }
        public Transform Transform { get { return _rootTransform; } }
        public Renderer Renderer { get { return _renderer; } }
        float ISelectable.MaxScale { get { return _maxScale; } }
        float ISelectable.MinScale { get { return _minScale; } }

        [Header("Internal references")]
        [SerializeField] private BoxCollider _boxCollider;
        [SerializeField] private Input.RaiseEventOnHit _clickHandler;
        [SerializeField] private Input.RaiseEventOnDoubleHit _doubleClickHandler;
        [SerializeField] private float _maxScale = 4f;
        [SerializeField] private MeshFilter _meshFilter;
        [SerializeField] private float _minScale = 0.2f;
        [SerializeField] private Transform _object3dTransform;
        [SerializeField] private Renderer _renderer;
        [SerializeField] private Rigidbody _rigidBody;
        [SerializeField] private Transform _rootTransform;

        private Object3dPort _object3dPort;

        internal void Init(Object3dPort object3dPort, CancellationToken cancellationToken)
        {
            _object3dPort = object3dPort;
            _object3dPort.Subscribe(this);
            Logic.AppService.Object3d.DTO.Object3d data = _object3dPort.GetData();
            TransformHelper.SetLocalPositionRotationScaleToTransform(Transform, data.Transform);
            _clickHandler.ClickEvent += OnClick;
            _doubleClickHandler.DoubleClickEvent += OnDoubleClick;
        }
        internal void InitVisuals(Mesh mesh)
        {
            _meshFilter.sharedMesh = mesh;
            Bounds newbound = _meshFilter.sharedMesh.bounds;
            _boxCollider.center = newbound.center;
            _boxCollider.size = newbound.size;
        }
        public void Dispose()
        {
            _object3dPort.Dispose();
            _object3dPort.Unsubscribe(this);
            Click = null;
            DoubleClick = null;
            _clickHandler.ClickEvent -= OnClick;
            _doubleClickHandler.DoubleClickEvent -= OnDoubleClick;
            Utility.Pool.Pool.Despawn(gameObject);
        }

        public Bounds GetBounds()
        {
            return _boxCollider.bounds;
        }
        internal void SetTransformToLogic()
        {
            _object3dPort.SetTransform(Common.TransformHelper.ToDtoTransform(Transform));
        }
        internal void SetColorToLogic()
        {
            var colorToSet = GradientHelper.ToDVColor(Renderer.material.color);
            Debug.Log($"{GetType().Name}.SetColorToLogic(): {colorToSet.ToString()} | Object ID: {Id}");
            _object3dPort.SetColor(colorToSet);
        }
        void IObject3dPortListener.OnTransformSet(Logic.AppService.Common.DTO.Transform transform)
        {
            TransformHelper.SetLocalPositionRotationScaleToTransform(Transform, transform);
        }
        void IObject3dPortListener.OnColorSet(Logic.AppService.Common.DTO.Gradient.Color color)
        {
            Renderer.material.color = GradientHelper.ToUnityColor(color);
        }

        private void OnClick()
        {
            Click?.Invoke(this);
        }
        private void OnDoubleClick()
        {
            DoubleClick?.Invoke(this);
        }
    }
}
