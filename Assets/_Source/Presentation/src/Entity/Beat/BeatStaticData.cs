using Presentation.Beat.Pool;
using UnityEngine;

namespace Presentation.Entity.Beat
{
    [CreateAssetMenu(menuName = "Beat/Data/Beat Data")]
    internal class BeatStaticData : ScriptableObject
    {
        public Objects3dPool Objects3d;
    }
}
