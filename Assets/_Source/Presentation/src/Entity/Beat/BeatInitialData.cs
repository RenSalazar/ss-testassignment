
namespace Presentation.Entity.Beat
{
    internal struct BeatInitialData
    {
        public BeatStaticData BeatStaticData { get; }

        public BeatInitialData(BeatStaticData beatStaticData)
        {
            BeatStaticData = beatStaticData;
        }
    }
}
