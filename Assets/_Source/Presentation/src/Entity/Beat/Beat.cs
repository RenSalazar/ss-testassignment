using System.Collections.Generic;
using System.Threading;
using Logic.AppService.Beat;
using Logic.AppService.Object3d;
using UnityEngine;

namespace Presentation.Entity.Beat
{
    [System.Flags]
    internal enum BeatComponent
    {
        None = 0,
        Objects3d = 1 << 1,

        All = Objects3d
    }
    internal interface IBeatPortProvider
    {
        IBeatEntityProvider BeatPort { get; }
    }
    internal sealed class Beat : MonoBehaviour, IBeatPortProvider, IBeatPortListener, System.IDisposable
    {
        private const float DOT_PRODUCT_TOLERANCE_VALUE = 0.0001f;

        internal delegate void ObjectAddedToTheBeatDelegate(Common.ISelectable selectable);
        internal delegate void ObjectRemovedFromTheBeatDelegate(Common.ISelectable selectable);

        internal event ObjectAddedToTheBeatDelegate ObjectAddedToTheBeatEvent;
        internal event ObjectRemovedFromTheBeatDelegate ObjectRemovedFromTheBeatEvent;
        internal event System.Action AllComponentsInitialized;

        public IBeatEntityProvider BeatPort => _beatPort;
        public Transform BeatCamera { get => _beatCameraTransform; }

        internal string Id { get { return _beatPort.Id; } }

        [SerializeField] private Transform _beatCameraTransform;
        [SerializeField] private Transform _parentFor3dObjects;
        [Header("Prefabs")]
        [SerializeField] private Object3d.Object3d _object3dPrefab;

        private BeatInitialData _beatInitialData;
        private Transform _transform;
        private BeatPort _beatPort;

        private Dictionary<string, Object3d.Object3d> _3dObjects;

        private BeatComponent _currentlyInitializedComponents = BeatComponent.None;

        internal void Init(BeatInitialData beatInitialData)
        {
            _transform = transform;
            _beatInitialData = beatInitialData;

            _3dObjects = _3dObjects ?? new Dictionary<string, Object3d.Object3d>();
        }

        internal void SetPort(BeatPort beatPort, CancellationToken cancellationToken)
        {
            _beatPort = beatPort;
            _beatPort.SetListener(this);
            _beatPort.Initialize3dObjectsAsync(cancellationToken);
        }
        internal void DisposeForReuse()
        {
            if (_beatPort != null)
            {
                _beatPort.Dispose();
            }
            foreach (var object3d in _3dObjects.Values)
            {
                object3d.Dispose();
            }
            _3dObjects.Clear();
            ObjectAddedToTheBeatEvent = null;
            ObjectRemovedFromTheBeatEvent = null;
            AllComponentsInitialized = null;
        }
        public void Dispose()
        {
            DisposeForReuse();
            //Destroy(gameObject);
            Utility.Pool.Pool.Despawn(gameObject);
        }

        internal Object3d.Object3d QueryObject3d(string id)
        {
            return _3dObjects[id];
        }
        internal void Get3dObjectSpawnPosition(out Vector3 beatSpacePosition, out Quaternion beatSpaceRotation, out Vector3 beatSpaceScale)
        {
            IntersectPointWithWallFacingTheCamera(out beatSpacePosition, out beatSpaceRotation);
            beatSpaceRotation = Quaternion.identity;
            beatSpaceScale = Common.TransformHelper.Snap10(Vector3.one);
        }
        private void IntersectPointWithWallFacingTheCamera(out Vector3 outPosition, out Quaternion outRotation)
        {
            Transform camTransform = _beatCameraTransform;
            outPosition = camTransform.position + camTransform.forward;
            outRotation = Quaternion.identity;
        }
        internal void SubscribeForObject3dClick(Common.ClickDelegate click)
        {
            foreach (Object3d.Object3d object3d in _3dObjects.Values)
            {
                object3d.Click += click;
            }
        }
        internal void UnSubscribeForObject3dClick(Common.ClickDelegate click)
        {
            foreach (Object3d.Object3d object3d in _3dObjects.Values)
            {
                object3d.Click -= click;
            }
        }
        internal Object3d.Object3d CreateObject3d(int indexInModels)
        {
            //Object3d.Object3d object3dGameObject = Instantiate(_object3dPrefab, _parentFor3dObjects);
            Object3d.Object3d object3dGameObject = Utility.Pool.Pool.Spawn(_object3dPrefab, _parentFor3dObjects);
            GameObject object3dTemplate = _beatInitialData.BeatStaticData.Objects3d.pool[indexInModels];
            Mesh mesh = object3dTemplate.GetComponent<MeshFilter>().sharedMesh;
            object3dGameObject.InitVisuals(mesh);
            return object3dGameObject;
        }

        void IBeatPortListener.OnObjects3dInitialized(Dictionary<string, Object3dPort> object3dPorts, CancellationToken cancellationToken)
        {
            foreach (var object3dIdPortPair in object3dPorts)
            {
                Object3dPort object3dPort = object3dIdPortPair.Value;
                string url = object3dPort.GetUrl();
                int indexInModels = int.Parse(url);
                Object3d.Object3d object3d = CreateObject3d(indexInModels);
                object3d.Init(object3dPort, cancellationToken);
                _3dObjects.Add(object3dIdPortPair.Key, object3d);
            }
            _currentlyInitializedComponents |= BeatComponent.Objects3d;
            PublishAllComponentsInitializedEventIfNecessary();
        }
        void IBeatPortListener.OnObject3dAdded(Object3dPort object3dPort, CancellationToken cancellationToken)
        {
            string url = object3dPort.GetUrl();
            int indexInModels = int.Parse(url);
            Object3d.Object3d object3d = CreateObject3d(indexInModels);
            object3d.Init(object3dPort, cancellationToken);
            _3dObjects.Add(object3dPort.Id, object3d);
            ObjectAddedToTheBeatEvent?.Invoke(object3d);
        }
        void IBeatPortListener.OnObject3dRemoved(string object3dId)
        {
            Object3d.Object3d object3d = _3dObjects[object3dId];
            object3d.Dispose();
            _3dObjects.Remove(object3dId);
        }

        private void PublishAllComponentsInitializedEventIfNecessary()
        {
            if (_currentlyInitializedComponents == BeatComponent.All && AllComponentsInitialized != null)
            {
                AllComponentsInitialized.Invoke();
            }
        }
    }
}
