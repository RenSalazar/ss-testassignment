using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Presentation.Entity.Common
{
    [Serializable]
    public class ColorGradient : IList<Color>
    {
        public bool IsColor { get; private set; }
        public bool IsGradient => !IsColor;

        public Color Color => IsColor ? _color : GradientColorKeys[0].color;

        public Gradient Gradient => _gradient;
        public Color ClearColor { get; set; }

        private GradientColorKey[] GradientColorKeys => _gradient.colorKeys;
        private readonly Gradient _gradient = new Gradient();
        private Color _color;

        public ColorGradient(Color color, Color clearColor)
        {
            IsColor = true;
            _color = color;
            ClearColor = clearColor;
            ClearGradient();
        }

        public ColorGradient(Gradient gradient, Color clearColor)
        {
            IsColor = false;
            ClearColor = clearColor;
            _color = ClearColor;
            _gradient = gradient;
        }
        
        public ColorGradient(Color[] colors, Color clearColor)
        {
            ClearColor = clearColor;
            if (colors.Length < 2)
            {
                IsColor = true;
                _color = colors.Length == 0 ? ClearColor : colors[0];
                ClearGradient();
                return;
            }
            GradientFromList(colors.ToList());
        }

        public IEnumerator<Color> GetEnumerator()
        {
            if (IsColor)
            {
                yield return _color;
                yield break;
            }
            for (int i = 0; i < GradientColorKeys.Length; i++)
            {
                yield return GradientColorKeys[i].color;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private void ClearGradient()
        {
            GradientColorKey[] colorKeys =
                {new GradientColorKey(ClearColor, 0f), new GradientColorKey(ClearColor, 1f)};
            _gradient.SetKeys(colorKeys, _gradient.alphaKeys);
        }

        private void SortTime(GradientColorKey[] colorKeys)
        {
            if (colorKeys.Length == 1)
            {
                colorKeys[0].time = 1;
                return;
            }
            for (int i = 0; i < colorKeys.Length; i++)
            {
                colorKeys[i].time = (float)i / (colorKeys.Length-1);
            }
        }

        private void GradientFromList(IReadOnlyList<Color> colorList)
        {
            if (colorList.Count < 2)
            {
                _color = colorList.Count == 0 ? ClearColor : colorList[0];
                IsColor = true;
                ClearGradient();
                return;
            }
            GradientColorKey[] colorKeys = new GradientColorKey[colorList.Count];
            for (int i = 0; i < colorKeys.Length; i++)
            {
                colorKeys[i] = new GradientColorKey(colorList[i], (float)i/(colorKeys.Length-1));
            }
            // its already sorted...
            _gradient.SetKeys(colorKeys, _gradient.alphaKeys);
            IsColor = false;
        }

        public void Add(Color item)
        {
            if (IsColor)
            {
                GradientColorKey[] colorKeys = {new GradientColorKey(Color, 0f), new GradientColorKey(item, 1f)};
                _gradient.SetKeys(colorKeys, Gradient.alphaKeys);
                IsColor = false;
            }
            else
            {
                GradientColorKey[] colorKeys = new GradientColorKey[GradientColorKeys.Length+1];
                GradientColorKeys.CopyTo(colorKeys, 0);
                colorKeys[GradientColorKeys.Length] = new GradientColorKey(item, 1f);
                SortTime(colorKeys);
                _gradient.SetKeys(colorKeys, _gradient.alphaKeys);
            }
        }

        public void Clear()
        {
            IsColor = true;
            _color = ClearColor;
            ClearGradient();
        }

        public bool Contains(Color item) => GradientColorKeys.Any(k => k.color == item);

        public void CopyTo(Color[] array, int arrayIndex) => GradientColorKeys.Select(k => k.color).ToArray().CopyTo(array, arrayIndex);

        public bool Remove(Color item)
        {
            int index = IndexOf(item);
            if (index == -1)
            {
                return false;
            }
            if (IsColor)
            {
                _color = ClearColor;
                return true;
            }
            List<Color> colorList = GradientColorKeys.Select(k => k.color).ToList();
            colorList.RemoveAt(index);
            GradientFromList(colorList);
            return true;
        }

        public int Count => IsColor ? 1 : GradientColorKeys.Length;
        public bool IsReadOnly => false;

        public int IndexOf(Color item)
        {
            if (IsColor)
            {
                if (Color == item)
                {
                    _color = ClearColor;
                    return 0;
                }
            }
            for (int i = 0; i < GradientColorKeys.Length; i++)
            {
                if (GradientColorKeys[i].color == item)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, Color item)
        {
            if (IsColor)
            {
                GradientColorKey[] colorKeys = GradientColorKeys;
                if (index > 0)
                {
                    throw new IndexOutOfRangeException();
                }
                colorKeys[0].color = item;
                colorKeys[1].color = Color;
                _gradient.SetKeys(colorKeys, _gradient.alphaKeys);
                IsColor = false;
                return;
            }
            List<Color> colorList = GradientColorKeys.Select(k => k.color).ToList();
            colorList.Insert(index, item);
            GradientFromList(colorList);
        }

        public void RemoveAt(int index)
        {
            if (IsColor)
            {
                if (index > 0)
                {
                    throw new IndexOutOfRangeException();
                }
                _color = ClearColor;
                return;
            }
            List<Color> colorList = GradientColorKeys.Select(k => k.color).ToList();
            colorList.RemoveAt(index);           
            GradientFromList(colorList);
        }

        public Color this[int index]
        {
            get
            {
                if (IsColor && index > 0)
                {
                    throw new InvalidOperationException();
                }
                return IsColor ? _color : GradientColorKeys[index].color;
            }
            set
            {
                if (IsColor)
                {
                    if (index == 0)
                    {
                        _color = value;
                        return;
                    }
                    throw new IndexOutOfRangeException();
                }
                GradientColorKey[] colorKeys = GradientColorKeys;
                colorKeys[index].color = value;
                _gradient.SetKeys(colorKeys, _gradient.alphaKeys);
            }
        }
    }
}
