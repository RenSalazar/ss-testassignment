
namespace Presentation.Entity.Common
{
    public delegate void ClickDelegate(ISelectable selectable);

    public interface ISelectable : System.IDisposable, IBoundable, IRenderable
    {
        event ClickDelegate Click;
        event ClickDelegate DoubleClick;

        string Id { get; }
        float MinScale { get; }
        float MaxScale { get; }
    }
}

