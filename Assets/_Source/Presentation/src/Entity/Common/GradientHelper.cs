using UnityEngine;
using ColorDTO = Logic.AppService.Common.DTO.Gradient.Color;
using GradientColorKeyDTO = Logic.AppService.Common.DTO.Gradient.GradientColorKey;
using GradientDTO = Logic.AppService.Common.DTO.Gradient.Gradient;
using ColorDV = Logic.Common.DataValues.Gradient.Color;

namespace Presentation.Entity.Common
{
    internal static class GradientHelper
    {
        public static ColorDTO ToDTOColor(Color color)
        {
            return new ColorDTO(color.r, color.g, color.b, color.a);
        }
        public static ColorDV ToDVColor(Color color)
        {
            return new ColorDV(color.r, color.g, color.b, color.a);
        }
        public static GradientColorKeyDTO ToDTOGradientColorKey(GradientColorKey gradientColorKey)
        {
            return new GradientColorKeyDTO(ToDTOColor(gradientColorKey.color), gradientColorKey.time);
        }
        public static GradientDTO ToDTOGradient(ColorGradient gradient)
        {
            if (gradient.IsColor)
            {
                return new GradientDTO(new GradientColorKeyDTO[1] { new GradientColorKeyDTO(ToDTOColor(gradient.Color), 0) });
            }
            GradientColorKey[] sourceGradientColorKeys = gradient.Gradient.colorKeys;
            GradientColorKeyDTO[] gradientColorKeys = new GradientColorKeyDTO[sourceGradientColorKeys.Length];
            for (int i = 0; i < gradientColorKeys.Length; ++i)
            {
                gradientColorKeys[i] = ToDTOGradientColorKey(sourceGradientColorKeys[i]);
            }
            return new GradientDTO(gradientColorKeys);
        }
        public static Color ToUnityColor(ColorDTO color)
        {
            return new Color(color.Red, color.Green, color.Blue, color.Alpha);
        }
        public static ColorGradient BuildColorGradient(ColorDTO color) => BuildColorGradient(ToUnityColor(color));

        public static ColorGradient BuildColorGradient(GradientDTO gradient)
        {
            Color[] colors = new Color[gradient.GradientColorKeys.Length];
            for (int i= 0; i< gradient.GradientColorKeys.Length; i++)
            {
                GradientColorKeyDTO gradientGradientColorKey = gradient.GradientColorKeys[i];
                colors[i] = ToUnityColor(gradientGradientColorKey.Color);
            }
            return new ColorGradient(colors, Color.white);
        }
        public static ColorGradient BuildColorGradient(Color color) => new ColorGradient(color, Color.white);
        public static ColorGradient BuildColorGradient(Color[] colors) => new ColorGradient(colors, Color.white);
        public static ColorGradient BuildColorGradient(Gradient gradient) => new ColorGradient(gradient, Color.white);
    }
}
