using UnityEngine;

namespace Presentation.Entity.Common
{
    public interface IBoundable
    {
        Bounds GetBounds();
        Transform Transform { get; }
    }
}
