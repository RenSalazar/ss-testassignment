﻿using UnityEngine;

namespace Presentation.Entity.Common
{

    public interface IRenderable
    {

        Renderer Renderer { get; }

    }

}