using UnityEngine;
using UnityEngine.UIElements;
using DTONamespace = Logic.AppService.Common.DTO;

namespace Presentation.Entity.Common
{
    internal static class TransformHelper
    {
        public static DTONamespace.Vector3 ToDtoVector(UnityEngine.Vector3 vector)
        {
            return new DTONamespace.Vector3(vector.x, vector.y, vector.z);
        }

        public static DTONamespace.Quaternion ToDtoQuaternion(UnityEngine.Quaternion quaternion)
        {
            return new DTONamespace.Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }

        public static DTONamespace.Transform ToDtoTransform(UnityEngine.Transform transform)
        {
            return new DTONamespace.Transform(
                ToDtoVector(transform.localPosition),
                ToDtoQuaternion(transform.localRotation),
                ToDtoVector(transform.localScale));
        }
        public static DTONamespace.Transform ToDtoTransform(UnityEngine.Vector3 position, UnityEngine.Quaternion rotation, UnityEngine.Vector3 scale)
        {
            return new DTONamespace.Transform(
                ToDtoVector(position),
                ToDtoQuaternion(rotation),
                ToDtoVector(scale));
        }

        public static UnityEngine.Vector3 ToUnityVector(DTONamespace.Vector3 vector)
        {
            return new UnityEngine.Vector3(vector.X, vector.Y, vector.Z);
        }
        public static UnityEngine.Quaternion ToUnityQuaternion(DTONamespace.Quaternion quaternion)
        {
            return new UnityEngine.Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
        }
        public static void SetLocalPositionRotationScaleToTransform(UnityEngine.Transform transform, DTONamespace.Transform values)
        {
            transform.localPosition = ToUnityVector(values.Position);
            transform.localRotation = ToUnityQuaternion(values.Rotation);
            transform.localScale = ToUnityVector(values.Scale);
        }
        public static UnityEngine.Vector3 Snap10(UnityEngine.Vector3 position)
        {
            position *= 10;
            position = new UnityEngine.Vector3(
                UnityEngine.Mathf.Round(position.x),
                UnityEngine.Mathf.Round(position.y),
                UnityEngine.Mathf.Round(position.z));
            position /= 10;
            return position;
        }
        public static UnityEngine.Vector3 Snap15(UnityEngine.Vector3 position)
        {
            position /= 15f;
            position = new UnityEngine.Vector3(
                UnityEngine.Mathf.Round(position.x),
                UnityEngine.Mathf.Round(position.y),
                UnityEngine.Mathf.Round(position.z));
            position *= 15f;
            return position;
        }

        public static UnityEngine.Vector3 Snap(UnityEngine.Vector3 v, float snap)
        {
            v /= snap;
            v.x = Mathf.RoundToInt(v.x);
            v.y = Mathf.RoundToInt(v.y);
            v.z = Mathf.RoundToInt(v.z);
            v *= snap;
            return v;
        }
    }
}
