using System.Collections.Generic;
using System.Threading;
using Logic.AppService.Creation.Panel;
using Presentation.Beat.Pool;
using Presentation.Entity.Common;
using UnityEngine;

namespace Presentation.UI.Creation.Category
{
    internal sealed class Object3d : MonoBehaviour, System.IDisposable
    {
        [SerializeField] private RectTransform _itemsParentRectTransform;
        [SerializeField] private Object3dItem _itemPrefab;

        private Entity.Beat.Beat _beat;
        private BeatObjectsPort _beatObjectsPort;
        private Objects3dPool _objects3dPool;
        private UI3D.SelectionFrame.Object3dSelectionFrame _selectionFrame;
        private CancellationTokenSource _cancellationTokenSource;

        private List<Object3dItem> _items;

        internal void Init(
            Entity.Beat.Beat beat,
            BeatObjectsPort beatObjectsPort,
            Objects3dPool objects3dPool,
            UI3D.SelectionFrame.Object3dSelectionFrame selectionFrame,
            CancellationTokenSource cancellationTokenSource)
        {
            _beat = beat;
            _beatObjectsPort = beatObjectsPort;
            _objects3dPool = objects3dPool;
            _selectionFrame = selectionFrame;
            _cancellationTokenSource = cancellationTokenSource;

            InitializeItems();
            gameObject.SetActive(true);
            _beat.ObjectAddedToTheBeatEvent += OnObjectAddedToTheBeat;

            _selectionFrame.Moved += SetTransformToLogic;
            _selectionFrame.Rotated += SetTransformToLogic;
            _selectionFrame.Scaled += SetTransformToLogic;
            _selectionFrame.Deleted += OnObject3dDeleteButtonClick;
            _selectionFrame.ColorChanged += OnColorChanged;
        }
        public void Dispose()
        {
            _beat.ObjectAddedToTheBeatEvent -= OnObjectAddedToTheBeat;

            _selectionFrame.Moved -= SetTransformToLogic;
            _selectionFrame.Rotated -= SetTransformToLogic;
            _selectionFrame.Scaled -= SetTransformToLogic;
            _selectionFrame.Deleted -= OnObject3dDeleteButtonClick;
            _selectionFrame.ColorChanged -= OnColorChanged;
            for (int i = 0; i < _items.Count; ++i)
            {
                _items[i].Dispose();
            }
        }

        private void InitializeItems()
        {
            for (int i = 0; i < _objects3dPool.pool.Count; ++i)
            {
                var object3d = _objects3dPool.pool[i];
                Object3dItem item = Instantiate(_itemPrefab, _itemsParentRectTransform);
                int closure = i;
                item.Init(object3d.name, () => { OnItemClick(closure); });
            }
        }
        private void OnItemClick(int index)
        {
            _beatObjectsPort.CreateObject3d(
                _beat.BeatPort,
                index.ToString(),
                Logic.AppService.Common.DTO.Vector3.Zero,
                Logic.AppService.Common.DTO.Vector3.One,
                Logic.AppService.Common.DTO.Quaternion.Identity,
                new Logic.Common.DataValues.Gradient.Color(),
                _cancellationTokenSource.Token);
        }
        private void OnObjectAddedToTheBeat(ISelectable selectable)
        {
            _selectionFrame.SelectObject(selectable);
            selectable.Click += OnObject3dClick;
        }
        private void OnObject3dClick(ISelectable selectable)
        {
            _selectionFrame.SelectObject(selectable);
        }

        private void SetTransformToLogic()
        {
            ISelectable selected = _selectionFrame.GetSelectedObject();
            Entity.Object3d.Object3d object3d = selected as Entity.Object3d.Object3d;
            if (object3d == null)
            {
                return;
            }
            object3d.SetTransformToLogic();
        }
        private void OnObject3dDeleteButtonClick(ISelectable selectable)
        {
            _beatObjectsPort.RemoveObject3d(_beat.BeatPort, selectable.Id);
        }
        private void OnColorChanged(ISelectable selectable)
        {
            ISelectable selected = _selectionFrame.GetSelectedObject();
            Entity.Object3d.Object3d object3d = selected as Entity.Object3d.Object3d;
            if (object3d == null)
            {
                return;
            }

            object3d.SetColorToLogic();
        }
    }
}
