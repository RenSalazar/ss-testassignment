using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.UI.Creation.Category
{
    internal sealed class Object3dItem : MonoBehaviour, System.IDisposable
    {
        [SerializeField] private Button _button;
        [SerializeField] private TextMeshProUGUI _buttonText;

        private Action _onItemClick;

        internal void Init(string text, Action onItemClick)
        {
            _buttonText.text = text;
            _onItemClick = onItemClick;
            _button.onClick.AddListener(OnButtonClick);
        }
        public void Dispose()
        {
            _button.onClick.RemoveListener(OnButtonClick);
            _onItemClick = null;
        }

        private void OnButtonClick()
        {
            _onItemClick?.Invoke();
        }
    }
}
