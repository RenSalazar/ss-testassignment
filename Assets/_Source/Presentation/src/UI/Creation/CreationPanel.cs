using System.Threading;
using Infrastructure.Context;
using Logic.AppService.Beat;
using Logic.AppService.Creation.Panel;
using Presentation.Utility.Pool;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.UI.Creation
{
    public class CreationPanel : MonoBehaviour, IServiceListener
    {
        [Header("Prefabs")]
        [SerializeField] private Entity.Beat.Beat _beatPrefab;
        [SerializeField] private UI3D.SelectionFrame.Object3dSelectionFrame _selectionFramePrefab;
        [Header("Internal references")]
        [SerializeField] private Entity.Beat.BeatStaticData _beatStaticData;
        [SerializeField] private Button _startCreationProcessButton;
        [SerializeField] private Category.Object3d _object3dCategory;
        [SerializeField] private Input.HitObjectOnScreen _hitObjectOnScreen;

        private UnityClientContext _unityClientContext;
        private UnityClientLocalContext _unityClientLocalContext;

        private Service _logic;
        private CancellationTokenSource _cancellationTokenSource;
        private Entity.Beat.Beat _beat;
        private UI3D.SelectionFrame.Object3dSelectionFrame _selectionFrame;

        private void Awake()
        {
            _unityClientContext = new UnityClientContext();
            _unityClientLocalContext = new UnityClientLocalContext();

            _startCreationProcessButton.onClick.AddListener(OnStartCreationProcessButtonClick);
            _selectionFrame = Instantiate(_selectionFramePrefab);
            _selectionFrame.Init();
        }
        private void OnApplicationQuit()
        {
            _startCreationProcessButton.onClick.RemoveListener(OnStartCreationProcessButtonClick);
            DestroyCancellationTokenSourceIfNotNull();
            DestroyBeatIfNotNull();
            _selectionFrame.Dispose();
        }

        void IServiceListener.OnDefaultCreationProcessStarted(BeatPort beatPort, BeatObjectsPort beatObjectsPort)
        {
            SetStartCreationProcessButtonEnabled(false);
            _beat.SetPort(beatPort, _cancellationTokenSource.Token);
            _selectionFrame.WorkWith(_beat);
            _object3dCategory.Init(
                _beat,
                beatObjectsPort,
                _beatStaticData.Objects3d,
                _selectionFrame,
                _cancellationTokenSource);
        }
        void IServiceListener.OnExitDefaultProcess()
        {
            SetStartCreationProcessButtonEnabled(true);

        }
        void IServiceListener.OnBeatPublished()
        {
            SetStartCreationProcessButtonEnabled(true);

        }

        private void OnStartCreationProcessButtonClick()
        {
            Entity.Beat.BeatInitialData beatInitialData = new Entity.Beat.BeatInitialData(_beatStaticData);
            DestroyBeatIfNotNull();
            _beat = Pool.Spawn(_beatPrefab);
            _beat.Init(beatInitialData);
            _hitObjectOnScreen.Camera = _beat.BeatCamera.GetComponent<Camera>();
            DestroyCancellationTokenSourceIfNotNull();
            _cancellationTokenSource = new CancellationTokenSource();
            _logic = new Service(_unityClientContext, _unityClientLocalContext, this);
            _logic.StartDefaultCreationProcess(_cancellationTokenSource.Token);
        }

        private void DestroyCancellationTokenSourceIfNotNull()
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                _cancellationTokenSource = null;
            }
        }
        private void DestroyBeatIfNotNull()
        {
            if (_beat != null)
            {
                _beat.Dispose();
            }
        }
        private void SetStartCreationProcessButtonEnabled(bool enabled)
        {
            _startCreationProcessButton.gameObject.SetActive(enabled);
        }
    }
}
