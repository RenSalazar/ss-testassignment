using System.Collections.Generic;
using UnityEngine;

namespace Presentation.Beat.Pool
{
    /// <summary>
    /// Asset to save a list of a certain Type so an object can access the objects from it 
    /// without saving all of them on itself.
    /// </summary>
    public class PoolBase<T> : ScriptableObject
    {
        [SerializeField]
        private bool _clearOnPlay = false;
        public List<T> pool = new List<T>();

        private void OnEnable()
        {
            if (_clearOnPlay)
                pool.Clear();
        }
    }
}
