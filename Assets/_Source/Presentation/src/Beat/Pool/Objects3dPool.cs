namespace Presentation.Beat.Pool

{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Beat/Pool/3D Objects")]
    public class Objects3dPool : PoolBase<GameObject> { }

}
