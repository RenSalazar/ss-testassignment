﻿namespace Presentation.Beat.Pool
{

    using UnityEngine;

    /// <summary>
    /// A ScriptableObject holding a list of Colors.
    ///
    /// - Renelie Salazar
    /// </summary>
    [CreateAssetMenu(menuName = "Beat/Pool/Color")]
    public class ColorPool : PoolBase<Color> { }

}