using Logic.AppService.Object3d.DTO;
using Logic.Common.BaseDomain.Repository;
using Logic.CoreDomain.Object3d;

namespace Infrastructure.Repository.Beat.Json
{
    public sealed class Object3dRepository : JsonRepository<IObject3dEntity>, IObject3dRepository
    {
        public Object3dRepository() :
            base(UnityEngine.Application.persistentDataPath, "Object3ds")
        {
        }

        protected override IObject3dEntity GetEntity(string json)
        {
            var dto = UnityEngine.JsonUtility.FromJson<Logic.AppService.Object3d.DTO.Object3d>(json);
            return Converter.ToEntity(dto);
        }

        protected override string GetJson(IObject3dEntity entity)
        {
            return UnityEngine.JsonUtility.ToJson(Converter.ToDataTransferObject(entity));
        }
    }
}
