using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Logic.Common.BaseDomain.Data;
using Logic.Common.BaseDomain.Repository;
using Logic.CoreDomain.Beat;

namespace Infrastructure.Repository.Beat.Json
{
    public sealed class BeatRepository : JsonRepository<IBeatEntity>, IBeatRepository
    {
        public BeatRepository() :
            base(UnityEngine.Application.persistentDataPath, "Beats")
        {
        }

        public Task<IReadOnlyList<IBeatEntity>> GetLastCreatedItemsAsync(int firstItemsCount, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
        public Task<IReadOnlyList<IBeatEntity>> GetNextLastCreatedItemsAsync(ID<IBeatEntity> startAfterId, int firstItemsCount, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        protected override IBeatEntity GetEntity(string json)
        {
            var beatDTO = UnityEngine.JsonUtility.FromJson<Logic.AppService.Beat.DTO.Beat>(json);
            return Logic.AppService.Beat.DTO.Converter.ToEntity(beatDTO);

        }
        protected override string GetJson(IBeatEntity entity)
        {
            return UnityEngine.JsonUtility.ToJson(Logic.AppService.Beat.DTO.Converter.ToDataTransferObject(entity));
        }
    }
}
