
namespace Infrastructure.Context
{
    public sealed class UnityClientLocalContext : UnityClientContext, Logic.AppService.Context.IContext
    {
        protected override void CreateBeatRepositories()
        {
            base.CreateBeatRepositories();
            //TODO: when the UnityClientContext will use remote repositories, use here local repositories (e.g. Json)
        }
    }
}
