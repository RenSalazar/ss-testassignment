using Logic.CoreDomain.Beat;
using Logic.CoreDomain.Object3d;
using RepositoryImpl = Infrastructure.Repository;

namespace Infrastructure.Context
{
    public class UnityClientContext : Logic.AppService.Context.IContext
    {
        public IBeatRepository BeatRepository { get; protected set; }
        public IObject3dRepository Object3dRepository { get; protected set; }


        public UnityClientContext()
        {
            CreateBeatRepositories();

        }
        public void Dispose()
        {
            BeatRepository.Dispose();
            Object3dRepository.Dispose();
        }

        protected virtual void CreateBeatRepositories()
        {
            BeatRepository = new RepositoryImpl.Beat.Json.BeatRepository();
            Object3dRepository = new RepositoryImpl.Beat.Json.Object3dRepository();
        }
    }
}
