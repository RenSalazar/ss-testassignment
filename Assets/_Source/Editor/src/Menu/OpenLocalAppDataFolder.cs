using UnityEditor;
using UnityEngine;

internal static class OpenLocalAppDataFolder
{
    [MenuItem("Soulside/Open Local App Data Folder")]
    private static void OnOpenLocalDataBasesFolder()
    {
        EditorUtility.RevealInFinder(Application.persistentDataPath);
    }
}
