using System.IO;
using System.Xml;
using UnityEditor;

namespace MyEditor.Postprocessor
{
    internal class UnityReferencesRemover : AssetPostprocessor
    {
        private static string OnGeneratedCSProject(string path, string content)
        {
            return RemoveAllUnityReferencesFromSpecificProjects(path, content);
        }

        private static string RemoveAllUnityReferencesFromSpecificProjects(string path, string content)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(content);

            RemoveAllChildNodesWithUnityReferenceAndReferenceOutputAssembly(path, xmlDoc.DocumentElement);

            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                xmlDoc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                string result = stringWriter.GetStringBuilder().ToString();
                return result;
            }
        }

        private static void RemoveAllChildNodesWithUnityReferenceAndReferenceOutputAssembly(string path, XmlNode parent)
        {
            if (parent == null)
                return;
            XmlNode child = parent.FirstChild;
            while (child != null)
            {
                RemoveAllChildNodesWithUnityReferenceAndReferenceOutputAssembly(path, child);
                if ((child.Name == "Reference") &&
                    (path.Contains("BaseDomain") ||
                    path.Contains("CoreDomain") ||
                    path.Contains("AppService") ||
                    path.Contains("DataValues")))
                {
                    string attrValue = child.Attributes["Include"].Value;
                    if (attrValue.Contains("Unity") && !attrValue.Equals("SyntaxTree.VisualStudio.Unity.Bridge"))
                    {
                        XmlNode toRemove = child;
                        child = child.NextSibling;
                        parent.RemoveChild(toRemove);
                        continue;
                    }
                }
                else if (child.Name == "ReferenceOutputAssembly")
                {// TODO: FIXME: revert last revision of this file, added this code because of
                 // Unity 2019.3.12f1 version, it generates this attribute and the VS build fails then
                    XmlNode toRemove = child;
                    child = child.NextSibling;
                    parent.RemoveChild(toRemove);
                    continue;
                }
                child = child.NextSibling;
            }
        }
    }
}
