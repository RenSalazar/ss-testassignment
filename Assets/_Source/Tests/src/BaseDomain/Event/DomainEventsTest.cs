using System.Threading;
using Logic.Common.BaseDomain.Event;
using NSubstitute;
using NUnit.Framework;

namespace Tests.BaseDomain.Event
{
    internal sealed class DomainEventsTest
    {
        public abstract class BaseDomainEvent : IDomainEvent
        {
            public CancellationToken CancellationToken { get; } = CancellationToken.None;
        }
        public sealed class FirstTestEvent : BaseDomainEvent { }
        public sealed class SecondTestEvent : BaseDomainEvent { }

        private readonly FirstTestEvent mTestEventOne = new FirstTestEvent();

        [Test]
        public void EventHandlerShouldReceiveHandleCallWhenEventIsFired()
        {
            var eventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(eventHandler);
            var eventToFire = new FirstTestEvent();

            DomainEvents.Add(eventToFire);
            DomainEvents.Fire();

            eventHandler.Received(1).HandleAsync(eventToFire);
        }

        [Test]
        public void EventHandlerShouldReceiveHandleCallOnlyForSpecificEventType()
        {
            var eventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(eventHandler);
            var eventToFire = new FirstTestEvent();

            DomainEvents.Add(eventToFire);
            DomainEvents.Add(new SecondTestEvent());
            DomainEvents.Fire();

            eventHandler.Received(1).HandleAsync(eventToFire);
        }

        [Test]
        public void EventHandlerShouldReceivedMultiplyTimesTheEventIfMultipleEventsWasAdded()
        {
            var eventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(eventHandler);
            var eventToFire = new FirstTestEvent();

            DomainEvents.Add(eventToFire);
            DomainEvents.Add(new FirstTestEvent());
            DomainEvents.Add(eventToFire);
            DomainEvents.Fire();

            eventHandler.ReceivedWithAnyArgs(3).HandleAsync(null);
        }

        [Test]
        public void TwoEventHandlersShouldReceiveTheAppropriateEventsThatTheySubscribed()
        {
            var firstEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            var secondEventHandler = Substitute.For<IHandle<SecondTestEvent>>();
            DomainEvents.Subscribe(firstEventHandler);
            DomainEvents.Subscribe(secondEventHandler);
            var firstEventToFire = new FirstTestEvent();
            var secondEventToFire = new SecondTestEvent();

            DomainEvents.Add(firstEventToFire);
            DomainEvents.Add(secondEventToFire);
            DomainEvents.Fire();

            firstEventHandler.Received(1).HandleAsync(firstEventToFire);
            secondEventHandler.Received(1).HandleAsync(secondEventToFire);
        }

        [Test]
        public void IfTheClearEventsIsCalledThenNoEventsIsFired()
        {
            var firstEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            var secondEventHandler = Substitute.For<IHandle<SecondTestEvent>>();
            DomainEvents.Subscribe(firstEventHandler);
            DomainEvents.Subscribe(secondEventHandler);
            var firstEventToFire = new FirstTestEvent();
            var secondEventToFire = new SecondTestEvent();

            DomainEvents.Add(firstEventToFire);
            DomainEvents.Add(secondEventToFire);
            DomainEvents.ClearEvents();
            DomainEvents.Fire();

            firstEventHandler.Received(0).HandleAsync(firstEventToFire);
            secondEventHandler.Received(0).HandleAsync(secondEventToFire);
        }

        [Test]
        public void IfTheClearHandlersIsCalledThenNoEventsIsFired()
        {
            var firstEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            var secondEventHandler = Substitute.For<IHandle<SecondTestEvent>>();
            DomainEvents.Subscribe(firstEventHandler);
            DomainEvents.Subscribe(secondEventHandler);
            var firstEventToFire = new FirstTestEvent();
            var secondEventToFire = new SecondTestEvent();

            DomainEvents.Add(firstEventToFire);
            DomainEvents.Add(secondEventToFire);
            DomainEvents.ClearHandlers();
            DomainEvents.Fire();

            firstEventHandler.Received(0).HandleAsync(firstEventToFire);
            secondEventHandler.Received(0).HandleAsync(secondEventToFire);
        }
        [Test]
        public void WhenASubscribedHandlerUnsubscribesItShouldNotReceiveHandleMessage()
        {
            var mockEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(mockEventHandler);
            DomainEvents.Add(mTestEventOne);
            DomainEvents.UnSubscribe(mockEventHandler);
            DomainEvents.Fire();

            mockEventHandler.Received(0).HandleAsync(mTestEventOne);
        }
        [Test]
        public void WhenASubscribedHandlerUnsubscribesTwiceItShouldNotReceiveHandleMessage()
        {
            var mockEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(mockEventHandler);
            DomainEvents.Add(mTestEventOne);
            DomainEvents.UnSubscribe(mockEventHandler);
            DomainEvents.UnSubscribe(mockEventHandler);
            DomainEvents.Fire();

            mockEventHandler.Received(0).HandleAsync(mTestEventOne);
        }
        [Test]
        public void WhenATwiceSubscribedHandlerUnsubscribesItShouldNotReceiveHandleMessage()
        {
            var mockEventHandler = Substitute.For<IHandle<FirstTestEvent>>();
            DomainEvents.Subscribe(mockEventHandler);
            DomainEvents.Subscribe(mockEventHandler);
            DomainEvents.Add(mTestEventOne);
            DomainEvents.Fire();

            mockEventHandler.Received(1).HandleAsync(mTestEventOne);
        }
    }
}
